# Multi-Language FE Modules and Global Multi-Language Layout Sections

## Multi Language Modules
This Contao 4 Module provides the ability, to reduce the ammount of modules beeing used in your installation.
You can create a "Global Module" and select a related Frontend-Module. Then select what fields are language aware and edit this fields for each language.

So more need to create a Module in a Theme for each language (e.g. jumpTo Pages).

This area is only editable for Administrators

## Global Layout Sections
You can create a Layout Section and assign a content type to it.
Then you may create multiple variants of this CE Element for your used languages.

Then grab the insert tag and put it in your template (fe_page or html module in your theme).

Why? You can have 1 Theme for a Multi-Language Site with language dependend Content.

E.g. on the your Website you have a different header image in french then in english. If so, this module is just right for you.


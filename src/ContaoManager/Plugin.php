<?php

/*
 * This file is part of pliigo/global-sections-bundle.
 *
 * (c) Johannes Pichler <j.pichler@webpixels.at>
 *
 * @license LGPL-3.0-or-later
 */

namespace Pliigo\GlobalSectionsBundle\ContaoManager;

use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Pliigo\GlobalSectionsBundle\PliigoGlobalSectionsBundle;

class Plugin implements BundlePluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(PliigoGlobalSectionsBundle::class)
                ->setLoadAfter([ContaoCoreBundle::class]),
        ];
    }
}

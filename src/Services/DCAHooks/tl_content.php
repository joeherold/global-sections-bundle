<?php

/*
 * This file is part of pliigo/global-sections-bundle.
 *
 * (c) Johannes Pichler <j.pichler@webpixels.at>
 *
 * @license LGPL-3.0-or-later
 */

namespace Pliigo\GlobalSectionsBundle\Services\DCAHooks;

use Contao\BackendUser;
use Contao\CoreBundle\Exception\AccessDeniedException;
// TODO SectionsModel use statement
use Contao\CoreBundle\Framework\ContaoFrameworkInterface;
use Contao\DataContainer;
use Michelf\MarkdownExtra;
use Pliigo\GlobalSectionsBundle\Models\SectionsModel;
use Pliigo\GlobalSectionsBundle\Traits\GlobalAccessRights;

class tl_content
{
    use GlobalAccessRights;
    /**
     * @var ContaoFrameworkInterface
     */
    private $framework;

    /**
     * Undocumented variable.
     *
     * @var BackendUser
     */
    private $User;

    private $Database;

    /**
     * __construct.
     *
     * @param mixed $framework
     */
    public function __construct(ContaoFrameworkInterface $framework)
    {
        $this->framework = $framework;
        if (!$this->framework->isInitialized()) {
            $this->framework->initialize();
        }

        $this->Database = \Contao\Database::getInstance();
        // $adapter = $this->framework->adapter("\Contao\BackendUser");
        $this->User = \BackendUser::getInstance();
    }

    /**
     * getParentHeader.
     *
     * @param mixed $row
     * @param mixed $dc
     */
    public function getParentHeader($row, $dc)
    {
        \System::loadLanguageFile('tl_pliigo_gbls_section');
        \System::loadLanguageFile('tl_content');

        // TODO does not work
        $parentItems = SectionsModel::findByPk($dc->id);
        // print_r($parentItems);
        // die();

        $desc = MarkdownExtra::defaultTransform($parentItems->description);

        // // print_r($dc->id);
        // // die();
        // return $row;
        return [
            $GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['name_group'][0] => $parentItems->name_group,
            $GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['name'][0] => $parentItems->name,
            $GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['description'][0] => sprintf('<div>%s</div>', $desc),
            $GLOBALS['TL_LANG']['tl_content']['type'][0] => sprintf('<div>%s</div>', $GLOBALS['TL_LANG']['CTE'][$parentItems->type][0].' - '.$GLOBALS['TL_LANG']['CTE'][$parentItems->type][1]),
        ];
    }

    public function renderContentType(DataContainer $dc)
    {
        $classes = $GLOBALS['TL_DCA']['tl_content']['fields']['type']['eval']['tl_class'];

        return '<div class="widget '.$classes.'">
            <h3 style="background-color: #0f1c26;color: #d3d5d7;border-left: 3px solid #f47c00;padding: 7px 10px 7px 10px;">'.$GLOBALS['TL_DCA']['tl_content']['fields']['type']['label'][0].'</h3>
            <div style="font-size: 14pt; padding: 12px 10px; border: 0px solid #efefef; color: #d3d5d7;background-color: #172b3b;border-left: 3px solid #f47c00;">'.$GLOBALS['TL_LANG']['CTE'][$dc->activeRecord->type][0].'</div>
            <p class="tl_help tl_tip" title="" style="background-color: #172b3b;color: #d3d5d7;border-left: 3px solid #f47c00;padding: 3px 10px 13px 10px;">'.$GLOBALS['TL_LANG']['CTE'][$dc->activeRecord->type][1].'</p>

        </div>';
    }

    public function getLangOpts()
    {
        $rootPages = \Contao\PageModel::findByPid(0);

        $languages = [];
        foreach ($rootPages as $page) {
            $languages[$page->language] = $page->language;
        }

        return array_unique($languages);
    }

    public function loadSaveParentValue($val, DataContainer $dc)
    {
        $sectionModel = SectionsModel::findByPk($dc->activeRecord->pid);

        // return "image";
        return $sectionModel->type;
    }

    /**
     * onload_callback: Fuehrt eine Aktion bei der Initialisierung des DataContainer-Objekts aus.
     *
     * @param $dc
     */
    public function limitInCaseOfGlobalSections(DataContainer $dc)
    {
        // do something ...
        if ('pliigo-gbls-sections-admin' === \Input::get('do') && 'tl_content' === \Input::get('table')) {
            // echo "<pre>";
            // print_r($GLOBALS['TL_DCA']['tl_content']);
            // $GLOBALS['TL_DCA']['tl_content']['list']['operations']['toggle']['attributes'] = 'onclick="Backend.getScrollOffset(); AjaxRequest.toggleVisibility(this,%s);location.reload();"';
            // $GLOBALS['TL_DCA']['tl_content']['list']['operations']['toggle']['attributes'] = rtrim($GLOBALS['TL_DCA']['tl_content']['list']['operations']['toggle']['attributes'], '"') . ';location.reload();"';

            if (TL_MODE === 'BE' && !\Input::get('act') && \Input::get('id')) {
                $sectionModel = SectionsModel::findByPk(\Input::get('id'));
                // die();
                if ($sectionMode && !$this->currentUsercanEditContent($sectionModel)) {
                    throw new AccessDeniedException('Not enough permissions for Layout Section '.$sectionModel->name.'.');
                }
            } elseif (TL_MODE === 'BE' && \Input::get('act') && $dc->id) {
                $cModel = \Contao\ContentModel::findByPk($dc->id);
                $sectionModel = SectionsModel::findByPk($cModel->pid);
                if ($sectionMode && !$this->currentUsercanEditContent($sectionModel)) {
                    throw new AccessDeniedException('Not enough permissions for Layout Section '.$sectionModel->name.'.');
                }
            }
        }
        if ('pliigo-gbls-sections-admin' === \Input::get('do') && \in_array(\Input::get('act'), ['edit', 'create'], true)) {
            $groups = [];

            $cModel = \Contao\ContentModel::findByPk($dc->id);

            // if($this->currentUsercanEditContent($cModel)){
            //     die();
            // }

            if ('tl_pliigo_gbls_section' === $cModel->ptable) {
                $sectionModel = SectionsModel::findByPk($cModel->pid);

                if ($sectionModel) {
                    // die();

                    if ($sectionModel->type !== $cModel->type) {
                        $cModel->type = $sectionModel->type;
                        $cModel->save();
                    }

                    $GLOBALS['TL_DCA']['tl_content']['fields']['type']['input_field_callback'] = ['pliigo.gbls.dca_hooks.tl_content', 'renderContentType'];
                    // $GLOBALS['TL_DCA']['tl_content']['fields']['type']['eval']['disabled'] = true;
                    $GLOBALS['TL_DCA']['tl_content']['fields']['type']['default'] = $sectionModel->type;
                    $GLOBALS['TL_DCA']['tl_content']['fields']['type']['load_callback'][] = ['pliigo.gbls.dca_hooks.tl_content', 'loadSaveParentValue'];
                    $GLOBALS['TL_DCA']['tl_content']['fields']['type']['save_callback'][] = ['pliigo.gbls.dca_hooks.tl_content', 'loadSaveParentValue'];

                    foreach ($GLOBALS['TL_DCA']['tl_content']['palettes'] as $key => $val) {
                        if ('__selector__' === $key) {
                            continue;
                        }

                        $GLOBALS['TL_DCA']['tl_content']['palettes'][$key] = str_replace(',type', ',type,pliigo_gbl_sections_lang', $GLOBALS['TL_DCA']['tl_content']['palettes'][$key]);
                        // $GLOBALS['TL_DCA']['tl_content']['palettes'][$key] = str_replace("type,", ",", $GLOBALS['TL_DCA']['tl_content']['palettes'][$key]);
                        // $GLOBALS['TL_DCA']['tl_content']['palettes'][$key] = str_replace("type;", ",", $GLOBALS['TL_DCA']['tl_content']['palettes'][$key]);
                    }

                    foreach ($GLOBALS['TL_CTE'] as $k => $v) {
                        foreach (array_keys($v) as $kk) {
                            // print_r($kk);
                            if ($kk !== $sectionModel->type) {
                                // unset($GLOBALS['TL_CTE'][$k][$kk]);
                            }

                            $groups[$k][] = $kk;
                        }
                    }
                }
            }
        }
    }

    /**
     * oncopy_callback: Wird ausgeführt nachdem ein Datensatz dupliziert wurde.
     *
     * @param mixed $insertID
     * @param mixed $dc
     */
    public function onCopyCallback($insertID, $dc)
    {
        // do something ...
        $cte = \Contao\ContentModel::findByPk($insertID);

        if ('tl_pliigo_gbls_section' === $cte->ptable) {
            $sectionModel = SectionsModel::findByPk($cte->pid);

            $cte->type = $sectionModel->type;
            $cte->save();
        }
    }

    /**
     * oncut_callback: Wird ausgeführt nachdem ein Datensatz verschoben wurde.
     *
     * @param mixed $dc
     */
    public function onCutCallback($dc)
    {
        $cte = \Contao\ContentModel::findByPk($dc->id);

        if ('tl_pliigo_gbls_section' === $cte->ptable) {
            $sectionModel = SectionsModel::findByPk($cte->pid);

            $cte->type = $sectionModel->type;
            $cte->save();
        }
    }

    public function getContentElement($objElem, $strBuffer)
    {
        if (TL_MODE === 'FE') {
            return $strBuffer;
        }

        $time = time();

        $sectionModel = SectionsModel::findByPk($objElem->pid);

        if ('pliigo-gbls-sections-admin' === \Contao\Input::get('do')) {
            $classIndicator = 'tl_green';
            $showError = false;

            try {
                $classIndicator = 'tl_green';
                $this->checkLanguageUnique($objElem->pliigo_gbl_sections_lang, $objElem);
            } catch (\Exception $e) {
                $classIndicator = 'tl_red';
                $showError = $e->getMessage();
                // \Contao\Message::addError($e->getMessage());
            }

            $returner = '';

            $returner .= '<div class="language-info">'.$GLOBALS['TL_LANG']['tl_content']['pliigo_gbl_sections']['label']['pliigo_gbl_sections_lang'][0].': <strong class="'.$classIndicator.'">'.($objElem->pliigo_gbl_sections_lang ?: '*').'</strong>'.($showError ? '<span class="tl_red"> - '.$showError.'</span>' : '').'</div>';

            $returner .= '<div class="publishing-info">';

            if ($objElem->start && $objElem->start > $time) {
                $returner .= ' <span class="visible_start tl_gray"> '.$GLOBALS['TL_LANG']['tl_content']['start'][0].': <span class="'.($objElem->start && $objElem->start > $time ? ' tl_red' : '').'">'.\Contao\Date::parse(\Contao\Date::getNumericDatimFormat(), $objElem->start).'</span></span>';
                if ($objElem->stop && $objElem->stop < $time) {
                    $returner .= '<span class="visible_start"> | </span>';
                }
            }
            if ($objElem->stop && $objElem->stop < $time) {
                $returner .= ' <span  class="visible_stop tl_gray"> '.$GLOBALS['TL_LANG']['tl_content']['stop'][0].': <span class="'.($objElem->start && $objElem->stop < $time ? ' tl_red' : '').'">'.\Contao\Date::parse(\Contao\Date::getNumericDatimFormat(), $objElem->stop).'</span></span>';
            }

            $returner .= '<span class=" not-visible-info'.($objElem->start && $objElem->start > $time ? ' not_visible_start' : '').($objElem->stop && $objElem->stop < $time ? ' not_visible_stop' : '').'">';
            $returner .= '<span class="not-visible-info-sepperator">&nbsp;|&nbsp;</span><small class="tl_red">NOT VISIBLE</small>';
            $$returner .= '</span>';
            $returner .= '</div>';

            $returner .= '<div>'.$strBuffer.'</div>
        ';

            return $returner;
        }

        return $strBuffer;
    }

    public function checkLanguageUnique($var, $dc)
    {
        if (!$dc->activeRecord) {
            $dc->activeRecord = \Contao\ContentModel::findByPk($dc->id);
        }
        if ($dc->activeRecord->invisible) {
            return $var;
        }
        $timeStamp = time();
        $sectionModel = SectionsModel::findByPk($dc->activeRecord->pid);

        $objCtes = $this->Database->prepare('SELECT id, start, stop FROM tl_content WHERE pid=? AND ptable=? AND pliigo_gbl_sections_lang=? AND id <>? AND invisible <> 1 AND (start = NULL OR start = 0 OR start = "" OR start < ?) AND (stop = NULL OR stop = 0 OR stop = "" OR stop > ?)')
            ->execute($dc->activeRecord->pid, 'tl_pliigo_gbls_section', $var, $dc->id, $timeStamp, $timeStamp);

        if ($objCtes->numRows > 0) {
            throw new \Exception('Duplicate Language for Element in Container '.$sectionModel->name.' in '.$sectionModel->name_group);
        }

        return $var;
    }
}

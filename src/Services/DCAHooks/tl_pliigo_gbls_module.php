<?php

/*
 * This file is part of pliigo/global-sections-bundle.
 *
 * (c) Johannes Pichler <j.pichler@webpixels.at>
 *
 * @license LGPL-3.0-or-later
 */

/*
 * SOME SHORTENERS FOR THE CODE.
 */

namespace Pliigo\GlobalSectionsBundle\Services\DCAHooks;

use Contao\BackendUser;
use Contao\Controller;
use Contao\CoreBundle\Framework\ContaoFrameworkInterface;
use Contao\ModuleModel;
use Pliigo\GlobalSectionsBundle\Models\SectionsModel;
use Pliigo\GlobalSectionsBundle\Models\SectionsModuleModel;
use Pliigo\GlobalSectionsBundle\Traits\GlobalAccessRights;

class tl_pliigo_gbls_module
{
    use GlobalAccessRights;

    protected $notAllowedActions = [
        'delete',
        'cut',
        'copy',
        'edit',
        'editcontent',
    ];
    protected $lockedDcActions = [
        'closed',
        'notEditable',
        'notDeletable',
        // 'notSortable',
        'notCopyable',
        'notCreateable',
    ];
    /**
     * @var ContaoFrameworkInterface
     */
    private $framework;

    /**
     * Undocumented variable.
     *
     * @var BackendUser
     */
    private $User;
    private $Database;

    /**
     * __construct.
     *
     * @param mixed $framework
     */
    public function __construct(ContaoFrameworkInterface $framework)
    {
        $this->framework = $framework;
        if (!$this->framework->isInitialized()) {
            $this->framework->initialize();
        }

        $this->Database = \Contao\Database::getInstance();
        // $adapter = $this->framework->adapter("\Contao\BackendUser");
        $this->User = \BackendUser::getInstance();
    }

    public function getFieldVariantOptions($dc)
    {
        //print_r($dc->activeRecord->module_to_use);

        $module = \Contao\ModuleModel::findByPk($dc->activeRecord->module_to_use);

        $type = $module->type;

        // print_r($type);
        Controller::loadDataContainer('tl_module');
        $palette = $GLOBALS['TL_DCA']['tl_module']['palettes'][$type];

        $arrPaletteByGroup = explode(';', $palette);

        $finalArrPalette = [];

        foreach ($arrPaletteByGroup as $key => $value) {
            $items = explode(',', $value);

            $arrPaletteByGroup[$key] = $items;
            $group = array_shift($items);
            preg_match('/\{(.*)\}/', $group, $matches);

            $groupName = preg_replace('/\:hide/', '', $matches[1]);
            $groupNameLang = $GLOBALS['TL_LANG']['tl_module'][$groupName];
            $finalArrPalette[(\is_array($groupNameLang) ? $groupNameLang[0] : $groupNameLang)] = [];

            foreach ($items as $item) {
                if ('type' === $item) {
                    continue;
                }
                $finalArrPalette[(\is_array($groupNameLang) ? $groupNameLang[0] : $groupNameLang)][$item] = $GLOBALS['TL_DCA']['tl_module']['fields'][$item]['label'][0];
            }
            //  $return[$k] = $GLOBALS['TL_DCA']['tl_member']['fields'][$k]['label'][0];
        }

        return $finalArrPalette;
        // echo "<pre>";
        // print_r($finalArrPalette);

        // die();
    }

    /**
     * loadLanguages.
     */
    public function loadLanguages()
    {
        \System::loadLanguageFile('tl_module');
        \System::loadLanguageFile('tl_theme');
    }

    /**
     * getParentHeader.
     */
    public function getParentHeader()
    {
        return [];
    }

    /**
     * getGroupHeader.
     *
     * @param mixed $group
     * @param mixed $mode
     * @param mixed $field
     * @param mixed $row
     */
    public function getGroupHeader($group, $mode, $field, $row)
    {
        return $row['name_group'] ? $row['name_group'] : '-';
        //    return "123";
    }

    /**
     * getRecordLabel.
     *
     * @param mixed $row
     * @param mixed $label
     */
    public function getRecordLabel($row, $label)
    {
        $clickerAppender = '';
        if ($this->User && $this->User->isAdmin) {
            $clicker = sprintf(
                ' <a title="simple insert tag" class="tl_gray" style="cursor: copy;" onclick=\'copyToClipboard("{{GBLS::module::%s}}" );return false;\'>{{ GBLS :: module :: %s }}</a>',
                $row['id'],
                $row['id']
            );

            $clickerWithTemplate = sprintf(
                ' <a title="extended insert tag with template" class="tl_gray" style="cursor: copy;" onclick=\'copyToClipboard( "{{GBLS::module::%s::custom_template}}" );return false;\'>{{ GBLS :: module :: %s :: custom_template }}</a>',
                $row['id'],
                $row['id']
            );
            $clickerAppender = $clicker.$clickerWithTemplate;
            // $clickerAppender = $clicker;

            // not used any more, because translation is triggered for every module
            $clickerAppender = '';
        }

        $time = time();
        $dbTable = 'tl_pliigo_gbls_module_variant';
        $result = $this->Database->prepare(
            "SELECT pliigo_language as lang
            FROM $dbTable 
            WHERE pid= ? 
           
            ORDER BY pliigo_language ASC
            ")->execute($row['id']);

        $langs = $result->fetchEach('lang');

        $langs = array_map(function ($val) {
            return $val ?: '*';
        }, $langs);
        // echo "<pre>";
        // print_r($langs);
        // die();
        $langsStr = implode(', ', $langs);
        $langStrErr = \count($langs) !== \count(array_unique($langs));

        //$GLOBALS['TL_LANG']['FMD']['wp_noe_divisions_and_persons']
        $rowType = $GLOBALS['TL_LANG']['FMD'][$row['type']][0];

        $module = ModuleModel::findByPk($row['module_to_use']);
        $rowType = $GLOBALS['TL_LANG']['FMD'][$module->type][0];
        // echo "<pre>";
        // // print_r($rowType);
        // print_r($GLOBALS['TL_LANG']['FMD']);
        // die();
        $rowTypeDesc = $GLOBALS['TL_LANG']['FMD'][$row['type']][1];
        $returner = sprintf('
        <div class="gbls layout-sections-record-row">
           <!-- <div class="first-col group-name"> %s </div>-->
           <!-- <div class="middle-col sepperator"> %s </div>-->
            <div class="middle-col name"> %s </div>
            <div class="middle-col type">&nbsp;[%s] </div>
            <div class="middle-col languages">&nbsp;%s </div>
            <div class="flex-grow"></div>
            <div class="last-col insert-tags"> %s </div>
        </div>
        ', ($row['name_group'] ? $row['name_group'] : ''), ($row['name_group'] ? ' ›› ' : ''), $row['name'], $rowType, ($langsStr ? '[<span class="'.($langStrErr ? 'tl_red' : 'tl_green').'">'.$langsStr.'</span>]' : ''), $clickerAppender);

        return $returner;
        // return '<span class="tl_gray">'.$row['name_group'].'</span> ›› '.$row['name'].' <span style="flex:1"></span> '.;
    }

    public function renderPliigoLogo()
    {
        return
            '<div class="widget clr long pliigo-dark-widget">
        <div class="pliigo-widget-container flex">
        <div class="pliigo-logo-container"></div>
        <div class="pliigo-widget-heading">
        <h2 >Pliigo Tools</h2>
        <span>&copy; Johannes Pichler &lt;j.pichler@webpixels.at&gt;</span>
        </div>
        </div>
     </div>';
    }

    public function renderModuleType(\Contao\DataContainer $dc)
    {
        // $moduleType = $dc->activeRecord->getRelated('module_to_use');
        // print_r($dc->activeRecord);
        if (\Input::post('module_to_use')) {
            $module = ModuleModel::findByPk(\Input::post('module_to_use'));
        } else {
            $module = ModuleModel::findByPk($dc->activeRecord->module_to_use);
        }

        // die();
        $classes = $GLOBALS['TL_DCA']['tl_module']['fields']['pliigoModuleType']['eval']['tl_class'];

        $titel = 'EDIT';
        $icon = 'modules.svg';
        $href = 'do=themes&table=tl_module&act=edit';
        if ($module && $module->id) {
            $edit = '<a href="'.\Backend::addToUrl($href.'&amp;id='.$module->id).'" title="'.specialchars($title).'"'.'>'.\Image::getHtml($icon, $module->module_to_use).'</a> ';
        } else {
            $edit = '';
        }

        return '<div class="widget clr w50'.$classes.'">
            <h3 style="background-color: #0f1c26;color: #d3d5d7;border-left: 3px solid #f47c00;padding: 7px 10px 7px 10px;">'.$GLOBALS['TL_DCA']['tl_module']['fields']['type']['label'][0].'&nbsp;'.$edit.'</h3>
            <div style="font-size: 14pt; padding: 12px 10px; border: 0px solid #efefef; color: #d3d5d7;background-color: #172b3b;border-left: 3px solid #f47c00;">'.$GLOBALS['TL_LANG']['FMD'][$module->type][0].'</div>
            <p class="tl_help tl_tip" title="" style="background-color: #172b3b;color: #d3d5d7;border-left: 3px solid #f47c00;padding: 3px 10px 13px 10px;">'.$GLOBALS['TL_LANG']['FMD'][$module->type][1].'</p>

        </div>';
    }

    /**
     * removeOperations.
     *
     * @param mixed $dc
     */
    public function removeOperations($dc)
    {
        if ($this->User->isAdmin) {
            return;
        }

        if (!$this->User->isAdmin) {
            foreach ($this->notAllowedActions as $action) {
                $GLOBALS['TL_DCA']['tl_pliigo_gbls_module']['list']['operations'][$action] = [];
            }
            foreach ($this->lockedDcActions as $action) {
                $GLOBALS['TL_DCA']['tl_pliigo_gbls_module']['config'][$action] = true;
            }
        }
    }

    /**
     * checkPermission.
     */
    public function checkPermission()
    {
    }

    /**
     * checkPermissionPerButton.
     *
     * @param mixed $arrRow
     * @param mixed $href
     * @param mixed $label
     * @param mixed $title
     * @param mixed $icon
     * @param mixed $attributes
     * @param mixed $strTable
     */
    public function checkPermissionForEditContentButton($arrRow, $href, $label, $title, $icon, $attributes, $strTable)
    {
        if ($this->currentUsercanEditModule($arrRow)) {
            return '<a href="'.\Backend::addToUrl($href.'&amp;id='.$arrRow['id']).'" title="'.specialchars($title).'"'.$attributes.'>'.\Image::getHtml($icon, $label).'</a> ';
        }

        return '<span>'.\Image::getHtml('edit_.svg', $label).'</span> ';

        return '';
    }

    public function goToModuleButtonCallback($arrRow, $href, $label, $title, $icon, $attributes, $strTable)
    {
        if ($this->currentUsercanEditModule($arrRow)) {
            return '<a href="'.\Backend::addToUrl($href.'&amp;id='.$arrRow['module_to_use']).'" title="'.specialchars($title).'"'.$attributes.'>'.\Image::getHtml($icon, $label).'</a> ';
        }

        return '<span>'.\Image::getHtml($icon, $label).'</span> ';

        return '';
    }

    /**
     * checkPermissionPerButton.
     *
     * @param mixed $arrRow
     * @param mixed $href
     * @param mixed $label
     * @param mixed $title
     * @param mixed $icon
     * @param mixed $attributes
     * @param mixed $strTable
     */
    public function checkPermissionPerButton($arrRow, $href, $label, $title, $icon, $attributes, $strTable)
    {
        if ($this->User->isAdmin) {
            return '<a href="'.\Backend::addToUrl($href).'" title="'.specialchars($title).'"'.$attributes.'>'.\Image::getHtml($icon, $label).'</a> ';
        }

        return '<a href="'.\Backend::addToUrl($href).'" title="'.specialchars($title).'"'.$attributes.'>'.\Image::getHtml($icon, $label).'</a> ';
    }

    /**
     * livePreviewMarkdown.
     */
    public function livePreviewMarkdown()
    {
        //'html_content =
        // return "123";
        return
            '<div class="widget w50">
        <h3>Live Render MarkDown</h3><br>
        <div class="pliigo-widget-container">
            <div id="markdown-preview-container">###</div>
        </div>
        <script>


            var tr = document.getElementById("ctrl_description");
            tr.addEventListener("change", renderMD);
            tr.addEventListener("keyUp", renderMD);
            tr.addEventListener("keyDown", renderMD);
            tr.addEventListener("cut", renderMD);
            tr.addEventListener("paste", renderMD);
            var me = document.getElementById("markdown-preview-container");


            function renderMD() {
                // console.log("ho honojlasd");
                me.innerHTML =  markdown.toHTML( tr.value );
            }
            renderMD();

            // setInterval(renderMD, 100);
            // var editor = ace.edit("ctrl_description");
            // editor.on("change", renderMD);
            // document.addEventListener(\'DOMContentLoaded\',function() {
            //     document.querySelector(\'#ctrl_description\').onchange=renderMD;
            // },false);
            document.addEventListener(\'keyup\', function(event) {
                if (event.defaultPrevented) {
                    return;
                }
                renderMD();

            });
        </script>
     </div>';
    }

    /**
     * Return all content elements as array.
     *
     * @return array
     */
    public function getContentElements()
    {
        $groups = [];

        foreach ($GLOBALS['TL_CTE'] as $k => $v) {
            foreach (array_keys($v) as $kk) {
                if (\in_array($kk, $GLOBALS['TL_WRAPPERS']['start'], true)) {
                    continue;
                }
                if (\in_array($kk, $GLOBALS['TL_WRAPPERS']['stop'], true)) {
                    continue;
                }
                if (\in_array($kk, $GLOBALS['TL_WRAPPERS']['single'], true)) {
                    continue;
                }
                $groups[$k][] = $kk;
            }
        }

        return $groups;
    }

    /**
     * Return the group of a content element.
     *
     * @param string $element
     *
     * @return string
     */
    public function getContentElementGroup($element)
    {
        foreach ($GLOBALS['TL_CTE'] as $k => $v) {
            foreach (array_keys($v) as $kk) {
                if ($kk === $element) {
                    return $k;
                }
            }
        }

        return null;
    }

    public function saveCallback($value, $dc)
    {
        $objCtes = $this->Database->prepare('SELECT id FROM '.SectionsModuleModel::getTable().' WHERE module_to_use=? AND id <>?')
            ->execute($value, $dc->id);

        // print_r($objCtes);
        // die();

        if ($objCtes->numRows > 0) {
            throw new \Exception('Duplicate usage of Module for global Module. Only use once');

            return '';
        }

        return $value;
    }

    /**
     * saveTypeToChildren.
     *
     * @param mixed $val
     * @param mixed $dc
     */
    public function saveTypeToChildren($val, $dc)
    {
        if ($dc && $dc->id) {
            $sectionModel = SectionsModel::findByPk($dc->id);
            $ctes = \Contao\ContentModel::findBy('ptable', 'tl_pliigo_gbls_section');
            foreach ($ctes as $cte) {
                if ($cte->pid === $sectionModel->id && $cte->type !== $val) {
                    $cte->type = $val;
                    $cte->save();
                }
            }
        }

        return $val;
    }

    /**
     * button_callback: Ermöglicht individuelle Navigationssymbole.
     *
     * @param array  $arrRow     the current row
     * @param string $href       the url of the embedded link of the button
     * @param string $label      label text for the button
     * @param string $title      title value for the button
     * @param string $icon       url of the image for the button
     * @param array  $attributes additional attributes for the button (fetched from the array key "attributes" in the DCA)
     * @param string $strTable   the name of the current table
     * @param $arrRootIds array of the ids of the selected "page mounts" (only in tree view)
     * @param $arrChildRecordIds ids of the childs of the current record (only in tree view)
     * @param bool   $blnCircularReference determines if this record has a circular reference (used to prevent pasting of an cutted item from an tree into any of it's childs)
     * @param string $strPrevious          id of the previous entry on the same parent/child level. Used for move up/down buttons. Not for root entries in tree view.
     * @param string $strNext              id of the next entry on the same parent/child level. Used for move up/down buttons. Not for root entries in tree view.
     *
     * @return var
     */
    public function checkIfIsAdmin($href, $label, $title, $icon, $attributes, $strTable)
    {
        if (!$this->User->isAdmin) {
            return '';
        }

        return '<a class="header_edit_all" href="'.\Contao\Backend::addToUrl($href, true).'" title="'.specialchars($title).'"'.$attributes.'>'.\Contao\Image::getHtml($icon).$label.'</a> ';
    }
}

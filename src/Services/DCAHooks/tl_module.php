<?php

/*
 * This file is part of pliigo/global-sections-bundle.
 *
 * (c) Johannes Pichler <j.pichler@webpixels.at>
 *
 * @license LGPL-3.0-or-later
 */

namespace Pliigo\GlobalSectionsBundle\Services\DCAHooks;

use Contao\BackendUser;
// TODO SectionsModel use statement
use Contao\CoreBundle\Framework\ContaoFrameworkInterface;
use Pliigo\GlobalSectionsBundle\Models\SectionsModuleModel;
use Pliigo\GlobalSectionsBundle\Models\SectionsModuleVariantModel;

class tl_module
{
    // use GlobalAccessRights;
    /**
     * @var ContaoFrameworkInterface
     */
    private $framework;

    /**
     * Undocumented variable.
     *
     * @var BackendUser
     */
    private $User;

    private $Database;

    /**
     * __construct.
     *
     * @param mixed $framework
     */
    public function __construct(ContaoFrameworkInterface $framework)
    {
        $this->framework = $framework;
        if (!$this->framework->isInitialized()) {
            $this->framework->initialize();
        }

        $this->Database = \Contao\Database::getInstance();
        // $adapter = $this->framework->adapter("\Contao\BackendUser");
        $this->User = \BackendUser::getInstance();
    }

    /**
     * List a front end module.
     *
     * @param array $row
     *
     * @return string
     */
    public function listModule($row)
    {
        $lAwareModule = SectionsModuleModel::findByModule_to_use($row['id']);

        /*
         * This file is part of pliigo/global-sections-bundle.
         *
         * (c) Johannes Pichler <j.pichler@webpixels.at>
         *
         * @license LGPL-3.0-or-later
         */

        $strBuffer = '';

        $strBuffer .= $row['name'].' <span style="color:#999;padding-left:3px">['.($GLOBALS['TL_LANG']['FMD'][$row['type']][0] ?? $row['type']).']</span>';

        if ($lAwareModule) {
            $children = SectionsModuleVariantModel::findByPid($lAwareModule->id, ['order' => 'pliigo_language ASC']);

            $strBuffer .= ' <span class="tl_green"> Global Module available:  [';
            // $strBuffer .= '<span class="tl_green"> Global Module available: </span>';
            if ($children) {
                $langs = $children->fetchEach('pliigo_language');
                $langs = array_map(function ($val) {
                    return $val ?: '*';
                }, $langs);
                $langs = implode(', ', $langs);

                $strBuffer .= "<span class=\"tl_green\">{$langs}</span>";
            } else {
                $strBuffer .= '<span class="tl_green"> Prepared for Languages, but yet no language created</span>';
            }
            // print_r($lAwareModule);
            // die();

            $strBuffer .= ' ]</span>';
        }

        return "<div class=\"tl_content_left\">{$strBuffer}</div>";
    }
}

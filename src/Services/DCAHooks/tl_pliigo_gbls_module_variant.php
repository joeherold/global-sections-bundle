<?php

/*
 * This file is part of pliigo/global-sections-bundle.
 *
 * (c) Johannes Pichler <j.pichler@webpixels.at>
 *
 * @license LGPL-3.0-or-later
 */

/*
 * SOME SHORTENERS FOR THE CODE.
 */

namespace Pliigo\GlobalSectionsBundle\Services\DCAHooks;

use Contao\BackendUser;
use Contao\CoreBundle\Exception\AccessDeniedException;
use Contao\CoreBundle\Framework\ContaoFrameworkInterface;
use Contao\Input;
use Contao\ModuleModel;
use Pliigo\GlobalSectionsBundle\Models\SectionsModel;
use Pliigo\GlobalSectionsBundle\Models\SectionsModuleModel;
use Pliigo\GlobalSectionsBundle\Models\SectionsModuleVariantModel;
use Pliigo\GlobalSectionsBundle\Traits\GlobalAccessRights;

class tl_pliigo_gbls_module_variant
{
    use GlobalAccessRights;
    /**
     * @var array Currently loaded data
     */
    protected $data = [];
    /**
     * @var array Data prepared for saving
     */
    protected $saveData = [];
    /**
     * @var array Fields configuration
     */
    protected $fieldsConfig = [];

    protected $notAllowedActions = [
        'delete',
        'cut',
        'copy',
        'edit',
    ];
    protected $lockedDcActions = [
        'closed',
        'notEditable',
        'notDeletable',
        // 'notSortable',
        'notCopyable',
        'notCreateable',
    ];
    /**
     * @var ContaoFrameworkInterface
     */
    private $framework;

    /**
     * Undocumented variable.
     *
     * @var BackendUser
     */
    private $User;
    private $Database;

    /**
     * __construct.
     *
     * @param mixed $framework
     */
    public function __construct(ContaoFrameworkInterface $framework)
    {
        $this->framework = $framework;
        if (!$this->framework->isInitialized()) {
            $this->framework->initialize();
        }

        $this->Database = \Contao\Database::getInstance();
        // $adapter = $this->framework->adapter("\Contao\BackendUser");
        $this->User = \BackendUser::getInstance();
    }

    public function listChildRecords($arrRow)
    {
        return $arrRow['pliigo_language'] ? $arrRow['pliigo_language'] : '*';
    }

    public function checkLanguageUnique($var, $dc)
    {
        // if ($var) {

        if (!$dc->activeRecord) {
            $dc->activeRecord = SectionsModuleVariantModel::findByPk($dc->id);
        }

        $sectionModel = SectionsModuleModel::findByPk($dc->activeRecord->pid);

        $objCtes = $this->Database->prepare('SELECT id FROM '.SectionsModuleVariantModel::getTable().' WHERE pid=? AND pliigo_language=? AND id <>?')
            ->execute($dc->activeRecord->pid, $var, $dc->id);

        // print_r($objCtes);
        // die();

        if ($objCtes->numRows > 0) {
            throw new \Exception('Duplicate Language for Module '.$sectionModel->name.' in '.$sectionModel->name_group);

            return '';
        }
        // }

        return $var;
    }

    public function onloadCallback($dc)
    {
        $this->loadLanguages();

        if (!$this->User->isAdmin && TL_MODE === 'BE') {
            throw new AccessDeniedException('Not enough permissions to create events in calendar ID '.Input::get('pid').'.');
        }

        if ('create' === \Input::get('act')) {
            return;
        }

        if ('edit' === \Input::get('act')) {
            $this->reloadConfig();

            $this->generatePalette($dc);

            // echo "<pre>";
            // // echo $dc->table;
            // // // print_r($GLOBALS['TL_DCA']['tl_pliigo_gbls_module_variant']['palettes']);
            // print_r($GLOBALS['TL_DCA']['tl_pliigo_gbls_module_variant']);
            // die();
        }

        if ('editAll' === \Input::get('act') || 'overrideAll' === \Input::get('act')) {
            return $this->createDcaMultiEdit($dc);
        }

        $createFromPost = \Input::post('FORM_SUBMIT') === $dc->table;
        $tmpField = null;
        // return;

        // $this->createDca($dc, $type, $createFromPost, $tmpField);
    }

    /**
     * tl_content, tl_module and tl_form_field DCA onsubmit callback.
     *
     * Creates empty arrays for empty lists if no data is available
     * (e.g. for new elements)
     *
     * @param \DataContainer $dc Data container
     */
    public function onsubmitCallback($dc)
    {
        // $type = $this->getDcaFieldValue($dc, 'type');
        // if (!$type || substr($type, 0, 5) !== 'rsce_') {
        //     return;
        // }

        if ('tl_pliigo_gbls_module_variant' === \Input::post('FORM_SUBMIT') && 'edit' === \Input::get('act')) {
            $pid = $dc->activeRecord->pid;
            $objParent = SectionsModuleModel::findByPk($pid);

            $arrDisabled = ['FORM_SUBMIT', 'REQUEST_TOKEN', 'FORM_FIELDS', 'save'];
            $arrEnabled = deserialize($objParent->field_variants, true);

            // print_r($arrEnabled);

            $filtered = array_filter(
                $_POST,
                function ($key) use ($arrEnabled) {
                    return \in_array($key, $arrEnabled, true);
                },
                ARRAY_FILTER_USE_KEY
            );
            // print_r(json_encode($filtered));
            $obj = SectionsModuleVariantModel::findByPk($dc->id);
            $obj->pliigo_data = json_encode($filtered);
            $obj->save();

            // print_r($obj);
            // die();
        }
    }

    public function saveMultiCallback($value, $dc)
    {
        // print_r($dc->id);
        // print_r([$dc->field, $value]);
        // die();
        $field = $dc->field; //substr($dc->field, strlen($dc->activeRecord->type . '_field_'));

        // echo "<pre>";
        // print_r($_POST);
        // echo $dc->id." ".$dc->table;
        $data = \Database::getInstance()
            ->prepare("SELECT pliigo_data FROM {$dc->table} WHERE id=?")
            ->execute($dc->id)
            ->pliigo_data;

        // echo "<pre>";
        // echo "\ndata\n";
        // print_r($data);
        // echo "\njsonencode data\n";
        // print_r(json_decode($data, true));
        $data = $data ? json_decode($data, true) : [];

        // if(is_array($value)) {
        //     $value = serialize($value);
        // }

        $data[$field] = $value;

        // echo "<pre>";

        // print_r(json_encode($data));

        $data = json_encode($data);

        // echo "\njsonencode data\n";
        // print_r(json_encode($data));

        \Database::getInstance()
            ->prepare("UPDATE {$dc->table} SET pliigo_data = ? WHERE id = ?")
            ->execute($data, $dc->id);

        // echo "<hr>";
        // print_r([
        //     "id" => $dc->id,
        //    "data" => $data,
        //    "value" => $value,
        //    "field" => $field
        //    ]);

        return '';
    }

    public function saveCallback($value, $dc)
    {
        // return null;
        return '';
    }

    /**
     * Field load callback.
     *
     * Finds the current value for the field
     *
     * @param string         $value Current value
     * @param \DataContainer $dc    Data container
     *
     * @return string Current value for the field
     */
    public function loadMultiCallback($value, $dc)
    {
        if (null !== $value) {
            return $value;
        }

        $field = $dc->field; //substr($dc->field, strlen($dc->activeRecord->type . '_field_'));

        $data = \Database::getInstance()
            ->prepare("SELECT pliigo_data FROM {$dc->table} WHERE id=?")
            ->execute($dc->id)
            ->pliigo_data;
        $data = $data ? json_decode($data, true) : [];

        if (isset($data[$field])) {
            $value = $data[$field];
        }

        return $value;
    }

    /**
     * Field load callback.
     *
     * Finds the current value for the field
     *
     * @param string         $value Current value
     * @param \DataContainer $dc    Data container
     *
     * @return string Current value for the field
     */
    public function loadCallback($value, $dc)
    {
        if (null !== $value) {
            return $value;
        }

        if (\Input::post($dc->field)) {
            return \Input::post($dc->field);
        }

        $data = json_decode($dc->activeRecord->pliigo_data, true);

        return $data[$dc->field];
    }

    /**
     * Call loadConfig and bypass the cache.
     *
     * @param mixed $arrLimitedFields
     * @param mixed $multiEdit
     */
    public static function reloadConfig($arrLimitedFields = [], $multiEdit = false)
    {
        return static::loadConfig(true, $arrLimitedFields, $multiEdit);
    }

    /**
     * Load the TL_CTE, FE_MOD and TL_FFL configuration and use caching if possible.
     *
     * @param bool  $bypassCache
     * @param mixed $arrLimitedFields
     * @param mixed $multiEdit
     */
    public static function loadConfig($bypassCache = false, $arrLimitedFields = [], $multiEdit = false)
    {
        // Don't load the config in the install tool
        try {
            if (
                \System::getContainer()->get('request_stack')
                && \System::getContainer()->get('request_stack')->getCurrentRequest()
                && \in_array(
                    \System::getContainer()->get('request_stack')->getCurrentRequest()->get('_route'),
                    ['contao_install', 'contao_backend_install'],
                    true
                )
            ) {
                return;
            }
        } catch (\Exception $exception) {
            return;
        }

        \Contao\Controller::loadLanguageFile('tl_module');
        \Contao\Controller::loadDataContainer('tl_module');
        foreach ($GLOBALS['TL_DCA']['tl_module']['fields'] as $key => $field) {
            if ('id' === $key) {
                continue;
            }

            if ('pid' === $key) {
                continue;
            }

            if ('tstamp' === $key) {
                continue;
            }

            if ('pliigo_data' === $key) {
                continue;
            }

            if ('pliigo_image' === $key) {
                continue;
            }

            if ('pliigo_module_type' === $key) {
                continue;
            }
            if ('type' === $key) {
                continue;
            }

            if (!$field['inputType']) {
                continue;
            }

            // echo "<pre>";
            //     print_r([$key, in_array($key, $arrLimitedFields)]);

            if ($arrLimitedFields && \count($arrLimitedFields) > 0 && !\in_array($key, $arrLimitedFields, true)) {
                // print_r($arrLimitedFields);
                continue;
            }

            $GLOBALS['TL_DCA']['tl_pliigo_gbls_module_variant']['fields'][$key] = $field;
            $GLOBALS['TL_DCA']['tl_pliigo_gbls_module_variant']['fields'][$key]['exclude'] = false;
            if (true === $multiEdit) {
                $GLOBALS['TL_DCA']['tl_pliigo_gbls_module_variant']['fields'][$key]['save_callback'][] = ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module_variant', 'saveMultiCallback'];
                $GLOBALS['TL_DCA']['tl_pliigo_gbls_module_variant']['fields'][$key]['load_callback'][] = ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module_variant', 'loadMultiCallback'];
            } else {
                $GLOBALS['TL_DCA']['tl_pliigo_gbls_module_variant']['fields'][$key]['save_callback'][] = ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module_variant', 'saveCallback'];
                $GLOBALS['TL_DCA']['tl_pliigo_gbls_module_variant']['fields'][$key]['load_callback'][] = ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module_variant', 'loadCallback'];
            }

            $GLOBALS['TL_DCA']['tl_pliigo_gbls_module_variant']['fields'][$key]['eval']['alwaysSave'] = true;
            $GLOBALS['TL_DCA']['tl_pliigo_gbls_module_variant']['fields'][$key]['eval']['doNotSaveEmpty'] = true;
        }
        // die();
        // echo "<pre>";
        // print_r($GLOBALS['TL_DCA']['tl_pliigo_gbls_module_variant']['fields']);
        // die();

        foreach ($GLOBALS['TL_DCA']['tl_module']['subpalettes'] as $key => $value) {
            $GLOBALS['TL_DCA']['tl_pliigo_gbls_module_variant']['subpalettes'][$key] = $value;
        }

        foreach ($GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'] as $key => $value) {
            if ('type' === $key) {
                continue;
            }

            $GLOBALS['TL_DCA']['tl_pliigo_gbls_module_variant']['palettes']['__selector__'][] = $value;
        }
    }

    public function generatePalette($dc, $arrLimitFields = null)
    {
        // print_r($dc); //TODO
        // die();
        if ($dc->id || $dc > 0) {
            $id = ($dc->id ? $dc->id : $dc);
            $obj = SectionsModuleVariantModel::findByPk($id);
            // print_r($obj);

            $objParent = SectionsModuleModel::findByPk($obj->pid);
            $objModule = ModuleModel::findByPk($objParent->module_to_use);
            // print_r(deserialize($objParent->field_variants));
            $allFields = $GLOBALS['TL_DCA']['tl_module']['palettes'][$objModule->type];
            $fieldGroups = \Contao\StringUtil::trimsplit(';', $allFields);
            $fields = deserialize($objParent->field_variants, true);
            $enabledFields = '';
            foreach ($fieldGroups as $fieldGroup => $items) {
                $arrItems = \Contao\StringUtil::trimsplit(',', $items);
                foreach ($arrItems as $item) {
                    if (stripos($item, '}') > 0) {
                        // print_r($item);
                        preg_match('/\{(.*)\}/', $item, $matches);
                        $groupName = preg_replace('/\:hide/', '', $matches[1]);
                        // print_r($matches[1]);

                        $GLOBALS['TL_LANG']['tl_pliigo_gbls_module_variant'][$groupName] = $GLOBALS['TL_LANG']['tl_module'][$groupName];

                        //     print_r($groupName);
                        //     echo "<pre>";
                        // print_r($GLOBALS['TL_LANG']['tl_pliigo_gbls_module']);
                        // die();
                        $enabledFields .= ';'.$item;
                    } else {
                        if (\in_array($item, $fields, true)) {
                            $enabledFields .= ','.$item;
                        } else {
                            continue;
                        }
                    }
                }
            }

            $GLOBALS['TL_DCA']['tl_pliigo_gbls_module_variant']['palettes']['default'] = 'pliigo_image;{language_legend},pliigo_module_type,pliigo_language'.$enabledFields.';';
            // print_r($GLOBALS['TL_DCA']['tl_pliigo_gbls_module_variant']['palettes']['default']);
            // die();
        }
        // $GLOBALS['TL_DCA']['tl_pliigo_gbls_module_variant']['palettes']['default'] = 'pliigoImage,headlinea,headline;{other_legend},name,;';
    }

    /**
     * loadLanguages.
     */
    public function loadLanguages()
    {
        \System::loadLanguageFile('tl_module');

        // foreach($GLOBALS['TL_LANG']['tl_module'] as $key => $value) {
        //     $GLOBALS['TL_LANG']['tl_pliigo_gbls_module_variant'][$key] = $value;
        // }
        // [$groupName] =
        // $GLOBALS['TL_LANG']['tl_module'][$groupName];
    }

    /**
     * getParentHeader.
     *
     * @param mixed $row
     * @param mixed $dc
     */
    public function getParentHeader($row, $dc)
    {
        \Contao\Controller::loadDataContainer('tl_module');
        $fields = $row[$GLOBALS['TL_LANG']['tl_pliigo_gbls_module']['field_variants'][0]];
        $arrFields = \Contao\StringUtil::trimsplit(',', $fields);
        $arrFields = array_map(function ($item) {
            $mandatory = '';
            if (true === $GLOBALS['TL_DCA']['tl_module']['fields'][$item]['eval']['mandatory']) {
                $mandatory = '<strong><span class="mandatory">*</span></strong>';
            }

            return $GLOBALS['TL_LANG']['tl_module'][$item][0].$mandatory;
        }, $arrFields);

        $row[$GLOBALS['TL_LANG']['tl_pliigo_gbls_module']['field_variants'][0]] = implode(', ', $arrFields);
        // $row["_data"] = json_encode($row);
        return $row;
    }

    /**
     * getGroupHeader.
     *
     * @param mixed $group
     * @param mixed $mode
     * @param mixed $field
     * @param mixed $row
     */
    public function getGroupHeader($group, $mode, $field, $row)
    {
        // return $row['name_group'] ? $row['name_group'] : '-';
        //    return "123";
    }

    /**
     * getRecordLabel.
     *
     * @param mixed $row
     * @param mixed $label
     */
    public function getRecordLabel($row, $label)
    {
        $clickerAppender = '';
        if ($this->User && $this->User->isAdmin) {
            $clicker = sprintf(
                ' <a title="simple insert tag" class="tl_gray" style="cursor: copy;" onclick=\'copyToClipboard("{{GBLS::%s}}" );return false;\'>{{ GBLS :: %s }}</a>',
                $row['id'],
                $row['id']
            );

            $clickerWithTemplate = sprintf(
                ' <a title="extended insert tag with template" class="tl_gray" style="cursor: copy;" onclick=\'copyToClipboard( "{{GBLS::%s::custom_template}}" );return false;\'>{{ GBLS :: %s :: custom_template }}</a>',
                $row['id'],
                $row['id']
            );
            $clickerAppender = $clicker.$clickerWithTemplate;
        }

        $time = time();
        $result = $this->Database->prepare(
            'SELECT pliigo_gbl_sections_lang as lang
            FROM tl_content
            WHERE ptable= ? and pid= ?
            AND invisible <> 1
            AND (start = NULL OR start = 0 OR start <= ?)
            AND (stop = NULL OR stop = 0 OR stop >= ?)
            ORDER BY sorting ASC
            ')->execute('tl_pliigo_gbls_section', $row['id'], $time, $time);

        $langs = $result->fetchEach('lang');

        $langs = array_map(function ($val) {
            return $val ?: '*';
        }, $langs);
        // echo "<pre>";
        // print_r($langs);
        // die();
        $langsStr = implode(', ', $langs);
        $langStrErr = \count($langs) !== \count(array_unique($langs));

        $rowType = $GLOBALS['TL_LANG']['CTE'][$row['type']][0];
        $rowTypeDesc = $GLOBALS['TL_LANG']['CTE'][$row['type']][1];
        $returner = sprintf('
        <div class="gbls layout-sections-record-row">
           <!-- <div class="first-col group-name"> %s </div>-->
           <!-- <div class="middle-col sepperator"> %s </div>-->
            <div class="middle-col name"> %s </div>
            <div class="middle-col type">&nbsp;[%s] </div>
            <div class="middle-col languages">&nbsp;%s </div>
            <div class="flex-grow"></div>
            <div class="last-col insert-tags"> %s </div>
        </div>
        ', ($row['name_group'] ? $row['name_group'] : ''), ($row['name_group'] ? ' ›› ' : ''), $row['name'], $rowType, ($langsStr ? '[<span class="'.($langStrErr ? 'tl_red' : 'tl_green').'">'.$langsStr.'</span>]' : ''), $clickerAppender);

        return $returner;
        // return '<span class="tl_gray">'.$row['name_group'].'</span> ›› '.$row['name'].' <span style="flex:1"></span> '.;
    }

    public function renderPliigoLogo()
    {
        return
            '<div class="widget clr long pliigo-dark-widget">
        <div class="pliigo-widget-container flex">
        <div class="pliigo-logo-container"></div>
        <div class="pliigo-widget-heading">
        <h2 >Pliigo Tools</h2>
        <span>&copy; Johannes Pichler &lt;j.pichler@webpixels.at&gt;</span>
        </div>
        </div>
     </div>';
    }

    public function renderModuleType(\Contao\DataContainer $dc)
    {
        // $moduleType = $dc->activeRecord->getRelated('module_to_use');

        $parentModel = SectionsModuleModel::findByPk($dc->activeRecord->pid);

        $module = ModuleModel::findByPk($parentModel->module_to_use);

        // die();
        $titel = 'EDIT';
        $icon = 'modules.svg';
        $href = 'do=themes&table=tl_module&act=edit';
        if ($module && $module->id) {
            $edit = '<a href="'.\Backend::addToUrl($href.'&amp;id='.$module->id).'" title="'.specialchars($title).'"'.'>'.\Image::getHtml($icon, $module->module_to_use).'</a> ';
        } else {
            $edit = '';
        }

        return '<div class="widget clr w50">
            <h3 style="background-color: #0f1c26;color: #d3d5d7;border-left: 3px solid #f47c00;padding: 7px 10px 7px 10px;">'.$GLOBALS['TL_DCA']['tl_module']['fields']['type']['label'][0].'&nbsp;'.$edit.'</h3>
            <div style="font-size: 14pt; padding: 8px 10px 12px 10px; border: 0px solid #efefef; color: #d3d5d7;background-color: #172b3b;border-left: 3px solid #f47c00;">'.$GLOBALS['TL_LANG']['FMD'][$module->type][0].'</div>
            <p class="tl_help tl_tip" title="" style="background-color: #172b3b;color: #d3d5d7;border-left: 3px solid #f47c00;padding: 3px 10px 13px 10px;">'.$GLOBALS['TL_LANG']['FMD'][$module->type][1].'</p>
            
            <div style="font-size: 10pt; padding: 12px 10px 12px 10px; border: 0px solid #efefef; color: #d3d5d7;background-color: #0f1c26;border-left: 3px solid #f47c00;font-style: italic;"><strong>'.$GLOBALS['TL_DCA']['tl_module']['fields']['name']['label'][0].'</strong>: '.$module->name.'</div>

        </div>';
    }

    /**
     * removeOperations.
     */
    public function removeOperations()
    {
        if ($this->User->isAdmin) {
            return;
        }

        if (!$this->User->isAdmin) {
            foreach ($this->notAllowedActions as $action) {
                $GLOBALS['TL_DCA']['tl_pliigo_gbls_section']['list']['operations'][$action] = [];
            }
            foreach ($this->lockedDcActions as $action) {
                $GLOBALS['TL_DCA']['tl_pliigo_gbls_section']['config'][$action] = true;
            }
        }
    }

    /**
     * checkPermission.
     */
    public function checkPermission()
    {
    }

    /**
     * checkPermissionPerButton.
     *
     * @param mixed $arrRow
     * @param mixed $href
     * @param mixed $label
     * @param mixed $title
     * @param mixed $icon
     * @param mixed $attributes
     * @param mixed $strTable
     */
    public function checkPermissionPerButton($arrRow, $href, $label, $title, $icon, $attributes, $strTable)
    {
        if ($this->User->isAdmin) {
            return '<a href="'.\Backend::addToUrl($href).'" title="'.specialchars($title).'"'.$attributes.'>'.\Image::getHtml($icon, $label).'</a> ';
        }

        return '<a href="'.\Backend::addToUrl($href).'" title="'.specialchars($title).'"'.$attributes.'>'.\Image::getHtml($icon, $label).'</a> ';
    }

    /**
     * livePreviewMarkdown.
     */
    public function livePreviewMarkdown()
    {
        //'html_content =
        // return "123";
        return
            '<div class="widget w50">
        <h3>Live Render MarkDown</h3><br>
        <div class="pliigo-widget-container">
            <div id="markdown-preview-container">###</div>
        </div>
        <script>


            var tr = document.getElementById("ctrl_description");
            tr.addEventListener("change", renderMD);
            tr.addEventListener("keyUp", renderMD);
            tr.addEventListener("keyDown", renderMD);
            tr.addEventListener("cut", renderMD);
            tr.addEventListener("paste", renderMD);
            var me = document.getElementById("markdown-preview-container");


            function renderMD() {
                // console.log("ho honojlasd");
                me.innerHTML =  markdown.toHTML( tr.value );
            }
            renderMD();

            // setInterval(renderMD, 100);
            // var editor = ace.edit("ctrl_description");
            // editor.on("change", renderMD);
            // document.addEventListener(\'DOMContentLoaded\',function() {
            //     document.querySelector(\'#ctrl_description\').onchange=renderMD;
            // },false);
            document.addEventListener(\'keyup\', function(event) {
                if (event.defaultPrevented) {
                    return;
                }
                renderMD();

            });
        </script>
     </div>';
    }

    /**
     * Return all content elements as array.
     *
     * @return array
     */
    public function getContentElements()
    {
        $groups = [];

        foreach ($GLOBALS['TL_CTE'] as $k => $v) {
            foreach (array_keys($v) as $kk) {
                if (\in_array($kk, $GLOBALS['TL_WRAPPERS']['start'], true)) {
                    continue;
                }
                if (\in_array($kk, $GLOBALS['TL_WRAPPERS']['stop'], true)) {
                    continue;
                }
                if (\in_array($kk, $GLOBALS['TL_WRAPPERS']['single'], true)) {
                    continue;
                }
                $groups[$k][] = $kk;
            }
        }

        return $groups;
    }

    /**
     * Return the group of a content element.
     *
     * @param string $element
     *
     * @return string
     */
    public function getContentElementGroup($element)
    {
        foreach ($GLOBALS['TL_CTE'] as $k => $v) {
            foreach (array_keys($v) as $kk) {
                if ($kk === $element) {
                    return $k;
                }
            }
        }

        return null;
    }

    /**
     * saveTypeToChildren.
     *
     * @param mixed $val
     * @param mixed $dc
     */
    public function saveTypeToChildren($val, $dc)
    {
        if ($dc && $dc->id) {
            $sectionModel = SectionsModel::findByPk($dc->id);
            $ctes = \Contao\ContentModel::findBy('ptable', 'tl_pliigo_gbls_section');
            foreach ($ctes as $cte) {
                if ($cte->pid === $sectionModel->id && $cte->type !== $val) {
                    $cte->type = $val;
                    $cte->save();
                }
            }
        }

        return $val;
    }

    /**
     * button_callback: Ermöglicht individuelle Navigationssymbole.
     *
     * @param array  $arrRow     the current row
     * @param string $href       the url of the embedded link of the button
     * @param string $label      label text for the button
     * @param string $title      title value for the button
     * @param string $icon       url of the image for the button
     * @param array  $attributes additional attributes for the button (fetched from the array key "attributes" in the DCA)
     * @param string $strTable   the name of the current table
     * @param $arrRootIds array of the ids of the selected "page mounts" (only in tree view)
     * @param $arrChildRecordIds ids of the childs of the current record (only in tree view)
     * @param bool   $blnCircularReference determines if this record has a circular reference (used to prevent pasting of an cutted item from an tree into any of it's childs)
     * @param string $strPrevious          id of the previous entry on the same parent/child level. Used for move up/down buttons. Not for root entries in tree view.
     * @param string $strNext              id of the next entry on the same parent/child level. Used for move up/down buttons. Not for root entries in tree view.
     *
     * @return var
     */
    public function checkIfIsAdmin($href, $label, $title, $icon, $attributes, $strTable)
    {
        if (!$this->User->isAdmin) {
            return '';
        }

        return '<a class="header_edit_all" href="'.\Contao\Backend::addToUrl($href, true).'" title="'.specialchars($title).'"'.$attributes.'>'.\Contao\Image::getHtml($icon).$label.'</a> ';
    }

    /**
     * Create all DCA standard fields for multi edit mode.
     *
     * @param \DataContainer $dc Data container
     */
    protected function createDcaMultiEdit($dc)
    {
        $session = \System::getContainer()->get('session')->all();
        if (empty($session['CURRENT']['IDS']) || !\is_array($session['CURRENT']['IDS'])) {
            return;
        }
        $ids = $session['CURRENT']['IDS'];
        $types = \Database::getInstance()
            ->prepare('
				SELECT id
				FROM '.$dc->table.'
				WHERE id IN ('.implode(',', $ids).')
				#	AND type LIKE \'rsce_%\'
				GROUP BY id
			')
            ->execute()
            ->fetchEach('id');
        if (!$types) {
            return;
        }

        $parentCollection = SectionsModuleVariantModel::findMultipleByIds($types);
        $compinedFields = [];
        foreach ($parentCollection as $item) {
        }

        $this->reloadConfig(['name', 'headline'], true);
        foreach ($types as $pid) {
            $paletteFields = [];

            // $objParent = SectionsModuleModel::getPyPk($pid);

            // $type = $objParent->type;

            $this->generatePalette($pid);
            // $config = static::getConfigByType($type);
            // if (!$config) {
            //     continue;
            // }
            // $standardFields = is_array($config['standardFields']) ? $config['standardFields'] : array();
            // foreach ($config['fields'] as $fieldName => $fieldConfig) {
            //     if (isset($fieldConfig['inputType']) && $fieldConfig['inputType'] !== 'list') {
            //         $this->createDcaItem($type . '_field_', $fieldName, $fieldConfig, $paletteFields, $dc, false, true);
            //     }
            // }
            // $GLOBALS['TL_DCA'][$dc->table]['palettes'][$type] = static::generatePalette(
            //     $dc->table,
            //     $paletteFields,
            //     $standardFields
            // );
        }
    }

    /**
     * Get the value of a field (from POST data, active record or the database).
     *
     * @param \DataContainer $dc        Data container
     * @param string         $fieldName Field name
     * @param bool           $fromDb    True to ignore POST data
     *
     * @return string The value
     */
    protected static function getDcaFieldValue($dc, $fieldName, $fromDb = false)
    {
        $value = null;
        if (\Input::post('FORM_SUBMIT') === $dc->table && !$fromDb) {
            $value = \Input::post($fieldName);
            if (null !== $value) {
                return $value;
            }
        }
        if ($dc->activeRecord) {
            $value = $dc->activeRecord->$fieldName;
        } else {
            $table = $dc->table;
            $id = $dc->id;
            if (\Input::get('target')) {
                $table = explode('.', \Input::get('target'), 2)[0];
                $id = (int) explode('.', \Input::get('target'), 3)[2];
            }
            if ($table && $id) {
                $record = \Database::getInstance()
                    ->prepare("SELECT * FROM {$table} WHERE id=?")
                    ->execute($id);
                if ($record->next()) {
                    $value = $record->$fieldName;
                }
            }
        }

        return $value;
    }
}

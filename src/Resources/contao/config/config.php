<?php

/*
 * This file is part of pliigo/global-sections-bundle.
 *
 * (c) Johannes Pichler <j.pichler@webpixels.at>
 *
 * @license LGPL-3.0-or-later
 */

$be_mod = [
    'pliigo-gbls' => [
        'pliigo-gbls-sections-admin' => [
            'tables' => ['tl_pliigo_gbls_section', 'tl_content'],
        ],
        'pliigo-gbls-modules-admin' => [
            'tables' => ['tl_pliigo_gbls_module', 'tl_pliigo_gbls_module_variant'],
        ],
        // 'pliigo-gbls-ce' => [
        //     'tables' => ['tl_pliigo_gbls_section', 'tl_content'],
        //    ],
        // 'pliigo-gbls-multi-lang-values' => [
        //     'tables' => ['tl_pliigo_gbls_ml_values'],
        // ],
    ],
];

// fix issue #1: only load styles and JS in BACKEND mode
if (TL_MODE && TL_MODE == "BE") {
    $GLOBALS['TL_CSS'][] = 'bundles/pliigoglobalsections/css/be_style.css';
    $GLOBALS['TL_JAVASCRIPT'][] = 'bundles/pliigoglobalsections/js/markdown.js';
    $GLOBALS['TL_JAVASCRIPT'][] = 'bundles/pliigoglobalsections/js/copy.js';
}
$GLOBALS['TL_MODELS']['tl_pliigo_gbls_section'] = 'Pliigo\GlobalSectionsBundle\Models\SectionsModel';
$GLOBALS['TL_MODELS']['tl_pliigo_gbls_module'] = 'Pliigo\GlobalSectionsBundle\Models\SectionsModuleModel';
$GLOBALS['TL_MODELS']['tl_pliigo_gbls_module_variant'] = 'Pliigo\GlobalSectionsBundle\Models\SectionsModuleVariantModel';

$GLOBALS['TL_HOOKS']['getContentElement'][] = ['pliigo.gbls.dca_hooks.tl_content', 'getContentElement'];
$GLOBALS['TL_HOOKS']['replaceInsertTags'][] = ['pliigo.gbls.listeners.render_inserttags', 'onReplaceInsertTags'];
$GLOBALS['TL_HOOKS']['getFrontendModule'][] = ['pliigo.gbls.listeners.render_inserttags', 'onGetFrontendModule'];
$GLOBALS['TL_HOOKS']['initializeSystem'][] = ['pliigo.gbls.listeners.initialize_system', 'onInitialize'];
array_insert($GLOBALS['BE_MOD'], 1, $be_mod);

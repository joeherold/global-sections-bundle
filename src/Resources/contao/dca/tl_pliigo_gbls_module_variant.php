<?php

/*
 * This file is part of pliigo/global-sections-bundle.
 *
 * (c) Johannes Pichler <j.pichler@webpixels.at>
 *
 * @license LGPL-3.0-or-later
 */

$GLOBALS['TL_DCA']['tl_pliigo_gbls_module_variant'] = [
    // Config
    'config' => [
        'dataContainer' => 'Table',
        'enableVersioning' => false,
        // 'ctable' => ['tl_content'],
        'ptable' => 'tl_pliigo_gbls_module',
        'switchToEdit' => false,
        'doNotCopyRecords' => false,
        // 'markAsCopy'                  => 'name',
        'onload_callback' => [
            ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module_variant', 'loadLanguages'],
            ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module_variant', 'onloadCallback'],
        ],
        'onsubmit_callback' => [
            ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module_variant', 'onsubmitCallback'],
        ],
        'sql' => [
            'keys' => [
                'id' => 'primary',
                // 'pid,pliigo_language' => 'unique',
            ],
        ],
    ],

    // List
    'list' => [
        'sorting' => [
            'mode' => 4,
            'fields' => ['pliigo_language'],
            'flag' => 0,
            'panelLayout' => 'filter;sort,search,limit',
            'headerFields' => ['module_to_use', 'field_variants'],
            'header_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module_variant', 'getParentHeader'],
            'child_record_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module_variant', 'listChildRecords'],
        ],
        'label' => [
            'fields' => ['name', 'id', 'id'],
            'format' => '%s <span class="tl_gray" style="cursor: copy;" onclick="copyToClipboard( \"{{gbls::module::%s}}\" )">{{ gbls :: module :: %s }}</span>',
             'group_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module_variant', 'getGroupHeader'],
            'label_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module_variant', 'getRecordLabel'],
        ],
        'global_operations' => [
            // 'new' => [
            //     'label' => &$GLOBALS['TL_LANG']['MSC']['new'],
            //     'href' => 'act=create',
            //     'class' => 'header_creates',
            //     'attributes' => 'onclick="Backend.getScrollOffset()" accesskey="e"',
            //     'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module_variant', 'checkPermissionPerButton']
            // ],
            'all' => [
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset()" accesskey="e"',
                'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module_variant', 'checkIfIsAdmin'],
            ],
        ],
        'operations' => [
            // 'editcontent' => [
            //     'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_module_variant']['edit'],
            //     'href' => 'table=tl_content',
            //     'icon' => 'edit.svg',
            //     //'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module_variant', 'checkPermissionPerButton']
            // ],
            'edit' => [
                'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_module_variant']['edit'],
                'href' => 'act=edit',
                'icon' => 'edit.svg',
                //'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module_variant', 'checkPermissionPerButton']
            ],
            'copy' => [
                'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_module_variant']['copy'],
                'href' => 'act=copy',
                'icon' => 'copy.svg',
                'attributes' => 'onclick="Backend.getScrollOffset()"',
                //'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module_variant', 'checkPermissionPerButton']
            ],
            // 'copyChilds' => array
            // (
            //     'label'               => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_module_variant']['copyChilds'],
            //     'href'                => 'act=paste&amp;mode=copy&amp;childs=1',
            //     'icon'                => 'copychilds.svg',
            //     'attributes'          => 'onclick="Backend.getScrollOffset()"',
            //     // 'button_callback'     => array('tl_page', 'copyPageWithSubpages')
            // ),
            // 'cut' => [
            //     'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_module_variant']['cut'],
            //     'href' => 'act=paste&amp;mode=cut',
            //     'icon' => 'cut.svg',
            //     'attributes' => 'onclick="Backend.getScrollOffset()"',
            //     //'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module_variant', 'checkPermissionPerButton']
            // ],
            'delete' => [
                'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_module_variant']['delete'],
                'href' => 'act=delete',
                'icon' => 'delete.svg',
                'attributes' => 'onclick="if(!confirm(\''.$GLOBALS['TL_LANG']['MSC']['deleteConfirm'].'\'))return false;Backend.getScrollOffset()"',
                //'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module_variant', 'checkPermissionPerButton']
            ],
            'show' => [
                'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_module_variant']['show'],
                'href' => 'act=show',
                'icon' => 'show.svg',
                // 'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module_variant', 'checkPermissionPerButton']
            ],
        ],
    ],

    // Palettes
    'palettes' => [
        //'__selector__' => ['wrap_in_div'],
        '__selector__' => [],
        'default' => 'pliigo_image;',
    ],

    // Subpalettes
    'subpalettes' => [
        // 'wrap_in_div' => 'cssID',
    ],

    // Fields
    'fields' => [
        'id' => [
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        'pid' => [
            'exclude' => true,
            'foreignKey' => 'tl_pliigo_gbls_module.name',
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'relation' => ['type' => 'belongsTo', 'load' => 'eager'],
        ],
        'tstamp' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'pliigo_image' => [
            'exclude' => true,
            'input_field_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module_variant', 'renderPliigoLogo'],
        ],
        'pliigo_module_type' => [
            'exclude' => true,
            'eval' => [
                'tl_class' => 'clr w50',
            ],
            'input_field_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module_variant', 'renderModuleType'],
        ],
        'pliigo_data' => [
            'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_module_variant']['pliigo_data'],
            'exclude' => true,
            // 'inputType' => 'rsce_list_hidden',
            'sql' => 'mediumblob NULL',
            'save_callback' => [
                // array('MadeYourDay\\RockSolidCustomElements\\CustomElements', 'saveDataCallback'),
            ],
        ],
        'pliigo_language' => [
            'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_module_variant']['pliigo_language'],
            'exclude' => true,
            // 'inputType' => 'select',
            'inputType' => 'text',

            'sql' => "char(255) not NULL default ''",
            'options_callback' => ['pliigo.gbls.dca_hooks.tl_content', 'getLangOpts'],
            'eval' => [
                'includeBlankOption' => true,
                'tl_class' => 'w50',
            ],
            'save_callback' => [
                ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module_variant', 'checkLanguageUnique'],
                // array('MadeYourDay\\RockSolidCustomElements\\CustomElements', 'saveDataCallback'),
            ],
        ],
    ],
];

<?php

/*
 * This file is part of pliigo/global-sections-bundle.
 *
 * (c) Johannes Pichler <j.pichler@webpixels.at>
 *
 * @license LGPL-3.0-or-later
 */

/**
 * SOME SHORTENERS FOR THE CODE.
 */
$dca_fields = &$GLOBALS['TL_DCA']['tl_content']['fields'];
$dca_config = &$GLOBALS['TL_DCA']['tl_content']['config'];
$dca_list = &$GLOBALS['TL_DCA']['tl_content']['list'];
$dca_palettes = &$GLOBALS['TL_DCA']['tl_content']['palettes'];
$dca_subpalettes = &$GLOBALS['TL_DCA']['tl_content']['subpalettes'];
$dca_palettes_keys = array_keys($dca_palettes);
$dca_subpalettes_keys = array_keys($dca_subpalettes);

$dca_fields['pliigo_gbl_sections_lang'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_content']['pliigo_gbl_sections']['label']['pliigo_gbl_sections_lang'],
    'inputType' => 'select',
    'eval' => [
        'tl_class' => 'w50',
        'includeBlankOption' => true,
    ],
    'sql' => "char(255) not NULL default ''",
    'save_callback' => [
        ['pliigo.gbls.dca_hooks.tl_content', 'checkLanguageUnique'],
    ],
    // 'load_callback' => array(
    //     ['pliigo.gbls.dca_hooks.tl_content', 'checkLanguageUnique']
    // ),

    'options_callback' => ['pliigo.gbls.dca_hooks.tl_content', 'getLangOpts'],
];

$dca_fields['pliigo_gbl_sections_lang_other'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_content']['pliigo_gbl_sections']['label']['pliigo_gbl_sections_lang_other'],
    'inputType' => 'select',
    'options_callback' => ['pliigo.gbls.dca_hooks.tl_content', 'getLangOpts'],
];

if ('pliigo-gbls-sections-admin' === \Input::get('do')) {
    $dca_config['ptable'] = 'tl_pliigo_gbls_section';
    $dca_list['sorting']['headerFields'] = ['name_group', 'name', 'description', 'type'];
    $dca_list['sorting']['header_callback'] = ['pliigo.gbls.dca_hooks.tl_content', 'getParentHeader'];
}
if ('pliigo-gbls-ce' === \Input::get('do')) {
    $dca_config['ptable'] = 'tl_pliigo_gbls_section';
}

$dca_config['onload_callback'][] = ['pliigo.gbls.dca_hooks.tl_content', 'limitInCaseOfGlobalSections'];
$dca_config['oncopy_callback'][] = ['pliigo.gbls.dca_hooks.tl_content', 'onCopyCallback'];
$dca_config['oncut_callback'][] = ['pliigo.gbls.dca_hooks.tl_content', 'onCutCallback'];

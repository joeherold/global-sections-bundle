<?php

/*
 * This file is part of pliigo/global-sections-bundle.
 *
 * (c) Johannes Pichler <j.pichler@webpixels.at>
 *
 * @license LGPL-3.0-or-later
 */

$GLOBALS['TL_DCA']['tl_pliigo_gbls_ml_values'] = [
    // Config
    'config' => [
        'dataContainer' => 'Table',
        'enableVersioning' => true,
        'ctable' => ['tl_content'],
        'switchToEdit' => false,
        // 'markAsCopy'                  => 'name',
        'onload_callback' => [
            ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_ml_values', 'removeOperations'],
            // array('tl_pliigo_gbls_ml_values', 'checkPermission'),
            // array('tl_pliigo_gbls_ml_values', 'addCustomLayoutSectionReferences')
        ],
        'sql' => [
            'keys' => [
                'id' => 'primary',
            ],
        ],
    ],

    // List
    'list' => [
        'sorting' => [
            'mode' => 1,
            'fields' => ['name'],
            // 'panelLayout' => 'filter;sort,search,limit',
            // 'headerFields' => ['name',  'tstamp'],
            // 'child_record_callback' => ['tl_pliigo_gbls_ml_values', 'listModule'],
        ],
        'label' => [
            'fields' => ['name'],
            'format' => '%s',

            // 'group_callback' => ['tl_pliigo_gbls_ml_values', 'getGroupHeader'],
        ],
        'global_operations' => [
            // 'new' => [
            //     'label' => &$GLOBALS['TL_LANG']['MSC']['new'],
            //     'href' => 'act=create',
            //     'class' => 'header_creates',
            //     'attributes' => 'onclick="Backend.getScrollOffset()" accesskey="e"',
            //     'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_ml_values', 'checkPermissionPerButton']
            // ],
            'all' => [
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset()" accesskey="e"',
                // 'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_ml_values', 'checkPermissionPerButton']
            ],
        ],
        'operations' => [
            'edit' => [
                'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_ml_values']['edit'],
                'href' => 'act=edit',
                'icon' => 'edit.svg',
                //'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_ml_values', 'checkPermissionPerButton']
            ],
            'copy' => [
                'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_ml_values']['copy'],
                'href' => 'act=copy',
                'icon' => 'copy.svg',
                'attributes' => 'onclick="Backend.getScrollOffset()"',
                //'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_ml_values', 'checkPermissionPerButton']
            ],
            // 'cut' => [
            //     'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_ml_values']['cut'],
            //     'href' => 'act=paste&amp;mode=cut',
            //     'icon' => 'cut.svg',
            //     'attributes' => 'onclick="Backend.getScrollOffset()"',
            //     //'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_ml_values', 'checkPermissionPerButton']
            // ],
            'delete' => [
                'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_ml_values']['delete'],
                'href' => 'act=delete',
                'icon' => 'delete.svg',
                'attributes' => 'onclick="if(!confirm(\''.$GLOBALS['TL_LANG']['MSC']['deleteConfirm'].'\'))return false;Backend.getScrollOffset()"',
                //'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_ml_values', 'checkPermissionPerButton']
            ],
            'show' => [
                'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_ml_values']['show'],
                'href' => 'act=show',
                'icon' => 'show.svg',
                // 'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_ml_values', 'checkPermissionPerButton']
            ],
        ],
    ],

    // Palettes
    'palettes' => [
        '__selector__' => ['wrap_in_div'],
        'default' => 'pliigoImage;{title_legend},name; description,markdown_preview_container;{expert_legend},wrap_in_div;',
    ],

    // Subpalettes
    'subpalettes' => [
        'wrap_in_div' => 'cssID',
    ],

    // Fields
    'fields' => [
        'id' => [
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        // 'pid' => [
        //     'exclude' => true,
        //     'foreignKey' => 'tl_theme.name',
        //     'sql' => "int(10) unsigned NOT NULL default '0'",
        //     'relation' => ['type' => 'belongsTo', 'load' => 'lazy'],
        // ],
        'tstamp' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'pliigoImage' => [
            // 'input_field_callback'=> function(){
            //     return '

            //     ';
            // }
            'exclude' => true,
            'input_field_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_ml_values', 'renderPliigoLogo'],
        ],
        'name' => [
            'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_ml_values']['name'],
            'exclude' => true,
            'sorting' => true,
            'flag' => 1,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'description' => [
            'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_ml_values']['name'],
            'exclude' => true,
            'sorting' => true,
            'flag' => 1,
            'search' => true,
            'inputType' => 'textarea',
            'eval' => [
                'mandatory' => true,
                'allowHtml' => true,
                'decodeEntities' => true,
                'preserveTags' => true,
                'nospace' => false,

                'rte' => 'ace|markdown',
                'tl_class' => 'clr monospace w50',
                ],

            'sql' => "text NOT NULL default ''",
        ],
        'markdown_preview_container' => [
            'exclude' => true,
            'input_field_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_ml_values', 'livePreviewMarkdown'],
        ],
        'wrap_in_div' => [
            'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_ml_values']['name'],
            'exclude' => true,
            // 'sorting' => true,
            // 'flag' => 1,
            // 'search' => true,
            'inputType' => 'checkbox',
            'eval' => ['tl_class' => 'clr w50'],
            'sql' => "char(1) NOT NULL default '0'",
        ],
        // 'headline' => [
        //     'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_ml_values']['headline'],
        //     'default' => ['value' => '', 'unit' => 'h2'],
        //     'exclude' => true,
        //     'search' => true,
        //     'inputType' => 'inputUnit',
        //     'options' => ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'],
        //     'eval' => ['maxlength' => 200, 'tl_class' => 'w50 clr'],
        //     'sql' => "varchar(255) NOT NULL default ''",
        // ],
        // 'type' => [
        //     'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_ml_values']['type'],
        //     'default' => 'navigation',
        //     'exclude' => true,
        //     'sorting' => true,
        //     'flag' => 11,
        //     'filter' => true,
        //     'inputType' => 'select',
        //     // 'options_callback' => ['tl_pliigo_gbls_ml_values', 'getModules'],
        //     'reference' => &$GLOBALS['TL_LANG']['FMD'],
        //     'eval' => ['helpwizard' => true, 'chosen' => true, 'submitOnChange' => true, 'tl_class' => 'w50'],
        //     'sql' => "varchar(64) NOT NULL default ''",
        // ],

        'cssID' => [
            'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_ml_values']['cssID'],
            'exclude' => true,
            'inputType' => 'text',
            'eval' => ['multiple' => true, 'size' => 2, 'tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
    ],
];

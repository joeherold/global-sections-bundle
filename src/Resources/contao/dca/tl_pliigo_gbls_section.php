<?php

/*
 * This file is part of pliigo/global-sections-bundle.
 *
 * (c) Johannes Pichler <j.pichler@webpixels.at>
 *
 * @license LGPL-3.0-or-later
 */

$GLOBALS['TL_DCA']['tl_pliigo_gbls_section'] = [
    // Config
    'config' => [
        'dataContainer' => 'Table',
        'enableVersioning' => true,
        'ctable' => ['tl_content'],
        'switchToEdit' => false,
        'doNotCopyRecords' => false,
        // 'markAsCopy'                  => 'name',
        'onload_callback' => [
            ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_section', 'loadLanguages'],
            ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_section', 'removeOperations'],
            // array('tl_pliigo_gbls_section', 'checkPermission'),
            // array('tl_pliigo_gbls_section', 'addCustomLayoutSectionReferences')
        ],
        'sql' => [
            'keys' => [
                'id' => 'primary',
            ],
        ],
    ],

    // List
    'list' => [
        'sorting' => [
            'mode' => 1,
            'fields' => ['name_group'],
            'flag' => 11,
            // 'panelLayout' => 'filter;sort,search,limit',
            // 'headerFields' => ['name',  'tstamp'],
            // 'child_record_callback' => ['tl_pliigo_gbls_section', 'listModule'],
        ],
        'label' => [
            'fields' => ['name', 'id', 'id'],
            'format' => '%s <span class="tl_gray" style="cursor: copy;" onclick="copyToClipboard( \"{{gbls::%s}}\" )">{{ gbls :: %s }}</span>',
            'group_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_section', 'getGroupHeader'],
            'label_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_section', 'getRecordLabel'],
        ],
        'global_operations' => [
            // 'new' => [
            //     'label' => &$GLOBALS['TL_LANG']['MSC']['new'],
            //     'href' => 'act=create',
            //     'class' => 'header_creates',
            //     'attributes' => 'onclick="Backend.getScrollOffset()" accesskey="e"',
            //     'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_section', 'checkPermissionPerButton']
            // ],
            'all' => [
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset()" accesskey="e"',
                'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_section', 'checkIfIsAdmin'],
            ],
        ],
        'operations' => [
            'editcontent' => [
                'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['edit'],
                'href' => 'table=tl_content',
                'icon' => 'edit.svg',
                'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_section', 'checkPermissionForEditContentButton'],
            ],
            'edit' => [
                'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['edit'],
                'href' => 'act=edit',
                'icon' => 'header.svg',
                //'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_section', 'checkPermissionPerButton']
            ],
            'copy' => [
                'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['copy'],
                'href' => 'act=copy',
                'icon' => 'copy.svg',
                'attributes' => 'onclick="Backend.getScrollOffset()"',
                //'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_section', 'checkPermissionPerButton']
            ],
            // 'copyChilds' => array
            // (
            // 	'label'               => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['copyChilds'],
            // 	'href'                => 'act=paste&amp;mode=copy&amp;childs=1',
            // 	'icon'                => 'copychilds.svg',
            // 	'attributes'          => 'onclick="Backend.getScrollOffset()"',
            // 	// 'button_callback'     => array('tl_page', 'copyPageWithSubpages')
            // ),
            // 'cut' => [
            //     'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['cut'],
            //     'href' => 'act=paste&amp;mode=cut',
            //     'icon' => 'cut.svg',
            //     'attributes' => 'onclick="Backend.getScrollOffset()"',
            //     //'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_section', 'checkPermissionPerButton']
            // ],
            'delete' => [
                'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['delete'],
                'href' => 'act=delete',
                'icon' => 'delete.svg',
                'attributes' => 'onclick="if(!confirm(\''.$GLOBALS['TL_LANG']['MSC']['deleteConfirm'].'\'))return false;Backend.getScrollOffset()"',
                //'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_section', 'checkPermissionPerButton']
            ],
            'show' => [
                'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['show'],
                'href' => 'act=show',
                'icon' => 'show.svg',
                // 'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_section', 'checkPermissionPerButton']
            ],
        ],
    ],

    // Palettes
    'palettes' => [
        '__selector__' => ['wrap_in_div', 'limitaccess'],
        'default' => 'pliigoImage;{title_legend},name_group,name;{description_language},description,markdown_preview_container;{limitaccess_legend},limitaccess;{expert_legend},type,,wrap_in_div;',
    ],

    // Subpalettes
    'subpalettes' => [
        'wrap_in_div' => 'cssID',
        'limitaccess' => 'disable_or_hide,allowed_users,allowed_groups',
    ],

    // Fields
    'fields' => [
        'id' => [
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        // 'pid' => [
        //     'exclude' => true,
        //     'foreignKey' => 'tl_theme.name',
        //     'sql' => "int(10) unsigned NOT NULL default '0'",
        //     'relation' => ['type' => 'belongsTo', 'load' => 'lazy'],
        // ],
        'tstamp' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'pliigoImage' => [
            // 'input_field_callback'=> function(){
            //     return '

            //     ';
            // }
            'exclude' => true,
            'input_field_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_section', 'renderPliigoLogo'],
        ],
        'name' => [
            'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['name'],
            'exclude' => true,
            'sorting' => true,
            'flag' => 1,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'name_group' => [
            'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['name_group'],
            'exclude' => true,
            'sorting' => true,
            'flag' => 11,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['mandatory' => false, 'maxlength' => 255, 'tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'description' => [
            'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['description'],
            'exclude' => true,
            'sorting' => true,
            'flag' => 1,
            'search' => true,
            'inputType' => 'textarea',
            "default"=> "",
            'eval' => [
                'mandatory' => false,
                'allowHtml' => true,
                'decodeEntities' => true,
                'preserveTags' => true,
                'nospace' => false,

                'rte' => 'ace|markdown',
                'tl_class' => 'clr monospace w50',
                ],

            'sql' => "text NOT NULL default ''",
        ],
        'markdown_preview_container' => [
            'exclude' => true,
            'input_field_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_section', 'livePreviewMarkdown'],
        ],
        'wrap_in_div' => [
            'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['wrap_in_div'],
            'exclude' => true,
            // 'sorting' => true,
            // 'flag' => 1,
            // 'search' => true,
            'inputType' => 'checkbox',
            'eval' => ['tl_class' => 'clr w50', 'submitOnChange' => true],
            'sql' => "char(1) NOT NULL default '0'",
        ],
        // 'headline' => [
        //     'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['headline'],
        //     'default' => ['value' => '', 'unit' => 'h2'],
        //     'exclude' => true,
        //     'search' => true,
        //     'inputType' => 'inputUnit',
        //     'options' => ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'],
        //     'eval' => ['maxlength' => 200, 'tl_class' => 'w50 clr'],
        //     'sql' => "varchar(255) NOT NULL default ''",
        // ],
        'type' => [
            'label' => &$GLOBALS['TL_LANG']['tl_content']['type'],
            'default' => 'text',
            'exclude' => true,
            'sorting' => true,
            'flag' => 11,
            'filter' => true,
            'inputType' => 'select',
            'options_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_section', 'getContentElements'],
            'reference' => &$GLOBALS['TL_LANG']['CTE'],
            'eval' => ['helpwizard' => true, 'mandatory' => 'true', 'chosen' => true, 'submitOnChange' => false, 'tl_class' => 'w50', 'doNotCopy' => false],
            'sql' => "varchar(64) NOT NULL default ''",
            'save_callback' => [
                ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_section', 'saveTypeToChildren'],
            ],
        ],

        'cssID' => [
            'label' => &$GLOBALS['TL_LANG']['tl_content']['cssID'],
            'exclude' => true,
            'inputType' => 'text',
            'eval' => ['multiple' => true, 'size' => 2, 'tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],

        'limitaccess' => [
            'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['limitaccess'],
            'exclude' => true,
            // 'sorting' => true,
            // 'flag' => 1,
            // 'search' => true,
            'inputType' => 'checkbox',
            'eval' => ['tl_class' => 'clr w50', 'submitOnChange' => true],
            'sql' => "char(1) NOT NULL default '0'",
        ],

        // 'disable_or_hide' => [
        //     'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['disable_or_hide'],
        //     'exclude' => true,
        //     // 'sorting' => true,
        //     // 'flag' => 1,
        //     // 'search' => true,
        //     'inputType' => 'select',
        //     'options' => ['disable', 'disable_and_hide'],
        //     'reference' => $GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['disable_or_hide'][2],
        //     'eval' => ['tl_class' => 'clr w50', 'submitOnChange' => true],
        //     'default' => "disable",
        //     'sql' => "char(255) NOT NULL default 'disable'",
        // ],

        'allowed_users' => [
            'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['allowed_users'],
            'exclude' => true,
            'inputType' => 'checkboxWizard',
            'foreignKey' => "tl_user.CONCAT(name, ' [', username, '] ', ' &lt;', email , '&gt; ')",
            'sql' => 'blob NULL',
            'relation' => ['type' => 'hasMany', 'load' => 'lazy'],
            'eval' => ['tl_class' => 'clr w50', 'multiple' => true],
        ],

        'allowed_groups' => [
            'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['allowed_groups'],
            'exclude' => true,
            'inputType' => 'checkboxWizard',
            'foreignKey' => 'tl_user_group.name',
            'sql' => 'blob NULL',
            'relation' => ['type' => 'hasMany', 'load' => 'lazy'],
            'eval' => ['tl_class' => 'w50', 'multiple' => true],
        ],
    ],
];

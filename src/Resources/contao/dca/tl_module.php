<?php

/*
 * This file is part of pliigo/global-sections-bundle.
 *
 * (c) Johannes Pichler <j.pichler@webpixels.at>
 *
 * @license LGPL-3.0-or-later
 */

$GLOBALS['TL_DCA']['tl_module']['list']['sorting']['child_record_callback'] = ['pliigo.gbls.dca_hooks.tl_module', 'listModule'];

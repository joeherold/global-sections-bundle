<?php

/*
 * This file is part of pliigo/global-sections-bundle.
 *
 * (c) Johannes Pichler <j.pichler@webpixels.at>
 *
 * @license LGPL-3.0-or-later
 */

$GLOBALS['TL_DCA']['tl_pliigo_gbls_module'] = [
    // Config
    'config' => [
        'dataContainer' => 'Table',
        'enableVersioning' => true,
         'ctable' => ['tl_pliigo_gbls_module_variant'],
        'switchToEdit' => false,
        'doNotCopyRecords' => false,
        // 'markAsCopy'                  => 'name',
        'onload_callback' => [
            ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module', 'loadLanguages'],
            ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module', 'removeOperations'],
            // array('tl_pliigo_gbls_module', 'checkPermission'),
            // array('tl_pliigo_gbls_module', 'addCustomLayoutSectionReferences')
        ],
        'sql' => [
            'keys' => [
                'id' => 'primary',
                'module_to_use' => 'unique',
            ],
        ],
    ],

    // List
    'list' => [
        'sorting' => [
            'mode' => 1,
            'fields' => ['name_group'],
            'flag' => 11,
            // 'panelLayout' => 'filter;sort,search,limit',
            // 'headerFields' => ['name',  'tstamp'],
            // 'child_record_callback' => ['tl_pliigo_gbls_module', 'listModule'],
        ],
        'label' => [
            'fields' => ['name', 'id', 'id'],
            'format' => '%s <span class="tl_gray" style="cursor: copy;" onclick="copyToClipboard( \"{{gbls::%s}}\" )">{{ gbls :: %s }}</span>',
            'group_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module', 'getGroupHeader'],
            'label_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module', 'getRecordLabel'],
        ],
        'global_operations' => [
            // 'new' => [
            //     'label' => &$GLOBALS['TL_LANG']['MSC']['new'],
            //     'href' => 'act=create',
            //     'class' => 'header_creates',
            //     'attributes' => 'onclick="Backend.getScrollOffset()" accesskey="e"',
            //     'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module', 'checkPermissionPerButton']
            // ],
            'all' => [
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset()" accesskey="e"',
                'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module', 'checkIfIsAdmin'],
            ],
        ],
        'operations' => [
            'editcontent' => [
                'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_module']['edit'],
                'href' => 'table=tl_pliigo_gbls_module_variant',
                'icon' => 'edit.svg',
                //'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module', 'checkPermissionPerButton']
            ],
            'edit' => [
                'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_module']['edit'],
                'href' => 'act=edit',
                'icon' => 'header.svg',
                //'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module', 'checkPermissionPerButton']
            ],

            'go_to_module' => [
                'label' => &$GLOBALS['TL_LANG']['tl_theme']['modules'],
                'href' => 'do=themes&table=tl_module&act=edit',
                'icon' => 'modules.svg', //id=2
                'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module', 'goToModuleButtonCallback'],
            ],
            'copy' => [
                'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_module']['copy'],
                'href' => 'act=copy',
                'icon' => 'copy.svg',
                'attributes' => 'onclick="Backend.getScrollOffset()"',
                //'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module', 'checkPermissionPerButton']
            ],
            // 'copyChilds' => array
            // (
            // 	'label'               => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_module']['copyChilds'],
            // 	'href'                => 'act=paste&amp;mode=copy&amp;childs=1',
            // 	'icon'                => 'copychilds.svg',
            // 	'attributes'          => 'onclick="Backend.getScrollOffset()"',
            // 	// 'button_callback'     => array('tl_page', 'copyPageWithSubpages')
            // ),
            // 'cut' => [
            //     'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_module']['cut'],
            //     'href' => 'act=paste&amp;mode=cut',
            //     'icon' => 'cut.svg',
            //     'attributes' => 'onclick="Backend.getScrollOffset()"',
            //     //'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module', 'checkPermissionPerButton']
            // ],
            'delete' => [
                'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_module']['delete'],
                'href' => 'act=delete',
                'icon' => 'delete.svg',
                'attributes' => 'onclick="if(!confirm(\''.$GLOBALS['TL_LANG']['MSC']['deleteConfirm'].'\'))return false;Backend.getScrollOffset()"',
                //'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module', 'checkPermissionPerButton']
            ],
            'show' => [
                'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_module']['show'],
                'href' => 'act=show',
                'icon' => 'show.svg',
                // 'button_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module', 'checkPermissionPerButton']
            ],
        ],
    ],

    // Palettes
    'palettes' => [
        '__selector__' => ['wrap_in_div'],
        'default' => 'pliigoImage;{title_legend},name_group,name;{expert_legend},pliigoModuleType,module_to_use,field_variants,wrap_in_div;',
    ],

    // Subpalettes
    'subpalettes' => [
        'wrap_in_div' => 'cssID',
    ],

    // Fields
    'fields' => [
        'id' => [
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        // 'pid' => [
        //     'exclude' => true,
        //     'foreignKey' => 'tl_theme.name',
        //     'sql' => "int(10) unsigned NOT NULL default '0'",
        //     'relation' => ['type' => 'belongsTo', 'load' => 'lazy'],
        // ],
        'tstamp' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'pliigoImage' => [
            'exclude' => true,
            'input_field_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module', 'renderPliigoLogo'],
        ],
        'pliigoModuleType' => [
            'exclude' => true,
            'eval' => [
                'tl_class' => 'clr w50',
            ],
            'input_field_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module', 'renderModuleType'],
        ],
        'name' => [
            'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_module']['name'],
            'exclude' => true,
            'sorting' => true,
            'flag' => 1,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'name_group' => [
            'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_module']['name_group'],
            'exclude' => true,
            'sorting' => true,
            'flag' => 11,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['mandatory' => false, 'maxlength' => 255, 'tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        // 'description' => [
        //     'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_module']['description'],
        //     'exclude' => true,
        //     'sorting' => true,
        //     'flag' => 1,
        //     'search' => true,
        //     'inputType' => 'textarea',
        //     'eval' => [
        //         'mandatory' => true,
        //         'allowHtml' => true,
        //         'decodeEntities' => true,
        //         'preserveTags' => true,
        //         'nospace' => false,

        //         'rte' => 'ace|markdown',
        //         'tl_class' => 'clr monospace w50',
        //         ],

        //     'sql' => "text NOT NULL default ''",
        // ],
        // 'markdown_preview_container' => [
        //     'exclude' => true,
        //     'input_field_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module', 'livePreviewMarkdown'],
        // ],

        'module_to_use' => [
            'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_module']['module_to_use'],
            'exclude' => true,
            // 'sorting' => true,
            // 'flag' => 1,
            // 'search' => true,
            'inputType' => 'select',

            'save_callback' => [
                ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module', 'saveCallback'],
            ],

            'eval' => ['tl_class' => 'w50', 'submitOnChange' => true, 'chosen' => true],
            'sql' => "int(10) unsigned NOT NULL default '0'",

            'foreignKey' => 'tl_module.name',
            'relation' => ['type' => 'hasOne', 'load' => 'eager'],
        ],

        'field_variants' => [
            'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_module']['field_variants'],
            'exclude' => true,
            'inputType' => 'checkbox',
            'eval' => ['tl_class' => 'clr w50', 'submitOnChange' => false, 'chosen' => true, 'multiple' => true],
            'options_callback' => ['pliigo.gbls.dca_hooks.tl_pliigo_gbls_module', 'getFieldVariantOptions'],
            'sql' => 'mediumblob NULL',
        ],

        'wrap_in_div' => [
            'label' => &$GLOBALS['TL_LANG']['tl_pliigo_gbls_module']['wrap_in_div'],
            'exclude' => true,
            // 'sorting' => true,
            // 'flag' => 1,
            // 'search' => true,
            'inputType' => 'checkbox',
            'eval' => ['tl_class' => 'clr w50', 'submitOnChange' => true],
            'sql' => "char(1) NOT NULL default '0'",
        ],

        'cssID' => [
            'label' => &$GLOBALS['TL_LANG']['tl_content']['cssID'],
            'exclude' => true,
            'inputType' => 'text',
            'eval' => ['multiple' => true, 'size' => 2, 'tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
    ],
];

<?php

/*
 * This file is part of pliigo/global-sections-bundle.
 *
 * (c) Johannes Pichler <j.pichler@webpixels.at>
 *
 * @license LGPL-3.0-or-later
 */

$GLOBALS['TL_LANG']['tl_content']['pliigo_gbl_sections']['label']['pliigo_gbl_sections_lang'] = [
    'Content für Sprache',
    'Wählen Sie bitte aus, für welche Sprache der Container angezeigt wird',
];

$GLOBALS['TL_LANG']['tl_content']['pliigo_gbl_sections']['ref']['pliigo_gbl_sections_lang'] = [
    'Content für Sprache',
    'Wählen Sie bitte aus, für welche Sprache der Container angezeigt wird',
];

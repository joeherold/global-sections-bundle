<?php

/*
 * This file is part of pliigo/global-sections-bundle.
 *
 * (c) Johannes Pichler <j.pichler@webpixels.at>
 *
 * @license LGPL-3.0-or-later
 */

$GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['new'] = ['Create new Section', 'Here you can create a new Section'];
 $GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['all'] = ['Edit multiple Sections at a time', 'Here you can create a new Section'];
 $GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['name'] = ['Name in Group', 'Give a Name in the Container Group'];
 $GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['name_group'] = ['Container Group', 'Give a name of the Container Group'];
 $GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['wrap_in_div'] = ['Wrap in Container', 'Wrap the insertet Element in a DIV container.'];
 $GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['description'] = ['Description', 'Give a reasonable Description for Content Creators.'];

 $GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['limitaccess'] = ['Limit User and Groups to access this Layout Element', 'Select specific Users or whole User Groups, to grant detailed Access Rights'];
 $GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['allowed_users'] = ['Allowed Users', 'Select specific Users to grant Access'];
 $GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['allowed_groups'] = ['Allowed Groups', 'Select specific Groups to grant Access'];
 $GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['disable_or_hide'] = [
     'Disable or Disable and Hide',
     'You can Deside, if the user even will see this Layout Element',
     [
         'disable' => 'disable this item for others',
         'disable_and_hide' => 'disable AND hide this item for others',
    ],
];
 $GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['allowed_groups'] = ['Allowed Groups', 'Select specific Groups to grant Access'];
 $GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['allowed_groups'] = ['Allowed Groups', 'Select specific Groups to grant Access'];

 // legend
 $GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['title_legend'] = 'Main Settings';
 $GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['expert_legend'] = 'Expert Settings';
 $GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['limitaccess_legend'] = 'Limit Access';
 $GLOBALS['TL_LANG']['tl_pliigo_gbls_section']['description_language'] = 'Description for Content Creators';

<?php

/*
 * This file is part of pliigo/global-sections-bundle.
 *
 * (c) Johannes Pichler <j.pichler@webpixels.at>
 *
 * @license LGPL-3.0-or-later
 */

$GLOBALS['TL_LANG']['tl_pliigo_gbls_module_variant']['new'] = ['Create new Language Variant', 'Here you can create a new global Module'];
 $GLOBALS['TL_LANG']['tl_pliigo_gbls_module_variant']['all'] = ['Edit multiple Sections at a time', 'Here you can create a new Section'];
 $GLOBALS['TL_LANG']['tl_pliigo_gbls_module_variant']['pliigo_language'] = ['Langauge to use module for', 'Please enter a language accrding to ISO-639-1'];

 // legend
 $GLOBALS['TL_LANG']['tl_pliigo_gbls_module_variant']['language_legend'] = 'Language Settings';

 $GLOBALS['TL_LANG']['tl_pliigo_gbls_module_variant']['type_legend'] = 'Main Settings';
 $GLOBALS['TL_LANG']['tl_pliigo_gbls_module_variant']['expert_legend'] = 'Expert Settings';
 $GLOBALS['TL_LANG']['tl_pliigo_gbls_module_variant']['description_language'] = 'Description for Content Creators';

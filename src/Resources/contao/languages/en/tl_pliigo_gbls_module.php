<?php

/*
 * This file is part of pliigo/global-sections-bundle.
 *
 * (c) Johannes Pichler <j.pichler@webpixels.at>
 *
 * @license LGPL-3.0-or-later
 */

$GLOBALS['TL_LANG']['tl_pliigo_gbls_module']['new'] = ['Create new Global Module', 'Here you can create a new global Module'];
 $GLOBALS['TL_LANG']['tl_pliigo_gbls_module']['all'] = ['Edit multiple Sections at a time', 'Here you can create a new Section'];
 $GLOBALS['TL_LANG']['tl_pliigo_gbls_module']['name'] = ['Name in Group', 'Give a Name in the Container Group'];
 $GLOBALS['TL_LANG']['tl_pliigo_gbls_module']['name_group'] = ['Container Group', 'Give a name of the Container Group'];
 $GLOBALS['TL_LANG']['tl_pliigo_gbls_module']['wrap_in_div'] = ['Wrap in Container', 'Wrap the insertet Module in a DIV container.'];
 $GLOBALS['TL_LANG']['tl_pliigo_gbls_module']['field_variants'] = ['Language depended fields', 'Please select the fields, that may vary in different languages.'];
 $GLOBALS['TL_LANG']['tl_pliigo_gbls_module']['module_to_use'] = ['Language dependend module', 'Please select the module, that has field varies in different languages.'];

 // legend
 $GLOBALS['TL_LANG']['tl_pliigo_gbls_module']['title_legend'] = 'Main Settings';
 $GLOBALS['TL_LANG']['tl_pliigo_gbls_module']['expert_legend'] = 'Expert Settings';
 $GLOBALS['TL_LANG']['tl_pliigo_gbls_module']['description_language'] = 'Description for Content Creators';

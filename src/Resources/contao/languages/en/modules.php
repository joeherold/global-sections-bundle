<?php

/*
 * This file is part of pliigo/global-sections-bundle.
 *
 * (c) Johannes Pichler <j.pichler@webpixels.at>
 *
 * @license LGPL-3.0-or-later
 */

 $GLOBALS['TL_LANG']['MOD']['pliigo-gbls'] = ['Pliigo Global Sections', 'Pliigo Global Sections Tool'];
 $GLOBALS['TL_LANG']['MOD']['pliigo-gbls-ce'] = ['Elements', 'Configure the layout sections'];
 $GLOBALS['TL_LANG']['MOD']['pliigo-gbls-sections-admin'] = ['Layout Sections', 'Configure the layout sections'];
 $GLOBALS['TL_LANG']['MOD']['pliigo-gbls-modules-admin'] = ['Global Modules', 'Configure the layout sections'];
 $GLOBALS['TL_LANG']['MOD']['pliigo-gbls-multi-lang-values'] = ['Language Context Values', 'Generate Names and words as multilang Words'];

; (function () {
    
    window.copyToClipboard = function (str) {
        var el = document.createElement('textarea');
        // el.value = "{{gbls :: "+str+" }}";
        el.value = str;
        el.setAttribute('readonly', '');
        el.style.position = 'absolute';
        el.style.left = '-9999px';
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
        window.alert(Pliigo.lang.inserttag_copied+"\n\n"+str);
        // alert("copied inserttag "+str+" to clipboard");
    };
})()


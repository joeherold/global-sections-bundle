<?php

/*
 * This file is part of pliigo/global-sections-bundle.
 *
 * (c) Johannes Pichler <j.pichler@webpixels.at>
 *
 * @license LGPL-3.0-or-later
 */

namespace Pliigo\GlobalSectionsBundle\Models;

use Contao\Model;

class SectionsModuleVariantModel extends Model
{
    protected static $strTable = 'tl_pliigo_gbls_module_variant';
}

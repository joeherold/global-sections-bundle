<?php

/*
 * This file is part of pliigo/global-sections-bundle.
 *
 * (c) Johannes Pichler <j.pichler@webpixels.at>
 *
 * @license LGPL-3.0-or-later
 */

namespace Pliigo\GlobalSectionsBundle\Traits;

use Contao\BackendUser;
use Contao\UserModel;
use Pliigo\GlobalSectionsBundle\Models\SectionsModel;
use Pliigo\GlobalSectionsBundle\Models\SectionsModuleModel;
use Twig\Error\Error;

trait GlobalAccessRights
{
    private $User;

    /**
     * currentUsercanEditModule.
     *
     * @param mixed $objGlobalModule
     * @param mixed $objGlobalSection
     */
    public function currentUsercanEditModule($objGlobalSection)
    {
        if (!($objGlobalSection instanceof SectionsModuleModel) && !(\is_array($objGlobalSection))) {
            // throw new \Exception('Please pass SectionsModuleModel or array of SectionsModuleModel');
            return false;
        }

        if ($objGlobalSection instanceof SectionsModuleModel) {
            $objGlobalSection = $objGlobalSection->row();
        }

        $this->User = BackendUser::getInstance();
        $objUser = UserModel::findByPk($this->User->id);
        $objUserGroups = $objUser->getRelated('groups');
        if ($this->User->isAdmin) {
            return true;
        }

        return false;

        // if ($objGlobalSection['limitaccess']) {
        //     $allowedUsers = deserialize($objGlobalSection['allowed_users'], true);
        //     $allowedGroups = deserialize($objGlobalSection['allowed_groups'], true);

        //     $userID = $this->User->id;
        //     $userGroups = deserialize($this->User->groups);

        //     if (\in_array($userID, $allowedUsers, true)) {
        //         return true;
        //     }

        //     if($objUserGroups && $objUserGroups->count() > 0){
        //         foreach ($allowedGroups as $index => $groupID) {
        //             if($this->User->isMemberOf($groupID)){

        //                 if(array_filter($objUserGroups->fetchAll(), function($group){

        //                     return !$group['disable'];
        //                 })[0]){
        //                     return true;
        //                 }

        //             }

        //         }
        //     }

        // }

        return false;
    }

    /**
     * currentUsercanEditContent.
     *
     * @param mixed $objGlobalSection
     */
    public function currentUsercanEditContent($objGlobalSection)
    {
        if (!($objGlobalSection instanceof SectionsModel) && !(\is_array($objGlobalSection))) {
            // throw new \Exception('Please pass SectionsModel or array of SectionsModel. You passed: '.\gettype($objGlobalSection));
            return false;
        }

        if ($objGlobalSection instanceof SectionsModel) {
            $objGlobalSection = $objGlobalSection->row();
        }

        $this->User = BackendUser::getInstance();
        $objUser = UserModel::findByPk($this->User->id);
        $objUserGroups = $objUser->getRelated('groups');
        if ($this->User->isAdmin) {
            return true;
        }

        if ($objGlobalSection['limitaccess']) {
            $allowedUsers = deserialize($objGlobalSection['allowed_users'], true);
            $allowedGroups = deserialize($objGlobalSection['allowed_groups'], true);

            $userID = $this->User->id;
            $userGroups = deserialize($this->User->groups);

            if (\in_array($userID, $allowedUsers, true)) {
                return true;
            }

            if ($objUserGroups && $objUserGroups->count() > 0) {
                foreach ($allowedGroups as $index => $groupID) {
                    if ($this->User->isMemberOf($groupID)) {
                        if (array_filter($objUserGroups->fetchAll(), function ($group) {
                            return !$group['disable'];
                        })[0]) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }
}

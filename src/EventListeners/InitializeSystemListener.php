<?php

/*
 * This file is part of pliigo/global-sections-bundle.
 *
 * (c) Johannes Pichler <j.pichler@webpixels.at>
 *
 * @license LGPL-3.0-or-later
 */

namespace Pliigo\GlobalSectionsBundle\EventListeners;

use Contao\BackendTemplate;
use Contao\System;

class InitializeSystemListener
{
    public function onInitialize()
    {
        if (TL_MODE === 'BE') {
            System::loadLanguageFile('pliigo_js');
            $be = new BackendTemplate('backend_js');

            $GLOBALS['TL_MOOTOOLS'][] = $be->parse();
        }
    }
}

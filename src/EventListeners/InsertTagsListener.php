<?php

/*
 * This file is part of pliigo/global-sections-bundle.
 *
 * (c) Johannes Pichler <j.pichler@webpixels.at>
 *
 * @license LGPL-3.0-or-later
 */

namespace Pliigo\GlobalSectionsBundle\EventListeners;

use Contao\ContentModel;
use Contao\CoreBundle\Framework\ContaoFrameworkInterface;
use Contao\ModuleModel;
use Pliigo\GlobalSectionsBundle\Models\SectionsModel;
use Pliigo\GlobalSectionsBundle\Models\SectionsModuleModel;
use Pliigo\GlobalSectionsBundle\Models\SectionsModuleVariantModel;

/**
 * InsertTagsListener class.
 */
class InsertTagsListener
{
    /**
     * @var ContaoFrameworkInterface
     */
    protected $framework;
    protected $Database;

    protected $supportedTags = [
        'gbls',
    ];

    /**
     * __construct function.
     *
     * @param ContaoFrameworkInterface $framework
     */
    public function __construct(ContaoFrameworkInterface $framework)
    {
        $this->framework = $framework;

        $this->Database = \Contao\Database::getInstance();
    }

    /**
     * onGetFrontendModule.
     *
     * @param ModuleModel $objRow
     * @param string      $strBuffer
     *
     * @return string
     */
    public function onGetFrontendModule(ModuleModel $objRow, $strBuffer = '')
    {
        // load the global page object to extract data of current page
        global $objPage;

        $sectionModels = SectionsModuleModel::findOneByModule_to_use($objRow->id);
        if ($sectionModels) {
            $modelToUse = null;

            $collLangVariantOfPage = SectionsModuleVariantModel::findBy(['pid=?', 'pliigo_language=?'], [$sectionModels->id, $objPage->rootLanguage], ['return' => 'Model']);
            if ($collLangVariantOfPage) {
                $modelToUse = $collLangVariantOfPage;
            } else {
                $collLangVariantDefault = SectionsModuleVariantModel::findBy(['pid=?', 'pliigo_language=? OR pliigo_language IS NULL'], [$sectionModels->id, ''], ['return' => 'Model']);
                if ($collLangVariantDefault) {
                    $modelToUse = $collLangVariantDefault;
                } else {
                    return $strBuffer;
                }
            }

            $modifiedData = json_decode($modelToUse->pliigo_data, true);

            if (!$modifiedData || empty($modifiedData) || 0 === \count($modifiedData)) {
                return $strBuffer;
            }

            $allowedFields = deserialize($sectionModels->field_variants, true);

            foreach ($modifiedData as $key => $data) {
                if (!\in_array($key, $allowedFields, true)) {
                    continue;
                }

                $objRow->{$key} = $data;
            }

            $objRow->{'pliigo_language'} = $modelToUse->pliigo_language;

            return $this->renderModifiedModule($objRow, $objRow->inColumn, $sectionModels->wrap_in_div ? true : false, $sectionModels->cssID);
        }

        return $strBuffer;
    }

    /**
     * onReplaceInsertTags function.
     *
     * @param string $tag
     *
     * @return string|false
     */
    public function onReplaceInsertTags($tag)
    {
        $elements = explode('::', $tag);
        $key = strtolower($elements[0]);
        $idOfElement = $elements[1];
        $customTemplate = $elements[2];
        switch ($key) {
            case 'gbls':
                return $this->replaceInsertTag($key, $idOfElement, $customTemplate);

                break;
            default:
                return false;
        }
    }

    /**
     * replaceInsertTags function.
     *
     * @param [type]     $tag
     * @param mixed      $idOfElement
     * @param mixed|null $customTemplate
     */
    public function replaceInsertTag($tag, $idOfElement = 0, $customTemplate = null)
    {
        if (!$idOfElement || $idOfElement < 1) {
            return false;
        }

        global $objPage;

        // $pageLanguage = $objPage->lang:
        $objSection = SectionsModel::findByPk($idOfElement);
        if (!$objSection) {
            return false;
        }
        $collesctionChildren = ContentModel::findPublishedByPidAndTable($objSection->id, SectionsModel::getTable(), ['order' => 'tl_content.sorting ASC']);

        if (!$collesctionChildren || !$collesctionChildren->count() > 0) {
            return $this->renderContentElementOfSection($objSection, null, $customTemplate);
        }
        $arrCollection = $collesctionChildren->fetchAll();

        $arrCollectionIds = $collesctionChildren->fetchEach('id');

        $arrCandiates = array_filter($arrCollection, function ($row) use ($objPage) {
            switch ($row['pliigo_gbl_sections_lang']) {
                case $objPage->rootLanguage:
                case $objPage->rootFallbackLanguage:
                case null:
                case '':
                    return true;
                default:
                    return false;
            }

            return false;
        });

        if (!empty($arrCandiates)) {
            $main = array_filter($arrCandiates, function ($row) use ($objPage) {
                if ($row['pliigo_gbl_sections_lang'] === $objPage->rootLanguage) {
                    return true;
                }

                return false;
            });
            if ($main && \count($main) > 0) {
                return $this->renderContentElementOfSection($objSection, array_shift($main)['id'], $customTemplate);
            }

            $fallback = array_filter($arrCandiates, function ($row) use ($objPage) {
                if ($row['pliigo_gbl_sections_lang'] === $objPage->rootFallbackLanguage) {
                    return true;
                }

                return false;
            });
            if ($fallback && \count($fallback) > 0) {
                return $this->renderContentElementOfSection($objSection, array_shift($fallback)['id'], $customTemplate);
            }
            $fallback2 = array_filter($arrCandiates, function ($row) use ($objPage) {
                if (null === $row['pliigo_gbl_sections_lang'] || !$row['pliigo_gbl_sections_lang'] || '' === $row['pliigo_gbl_sections_lang']) {
                    return true;
                }

                return false;
            });
            // echo "<pre>";
            // print_r($fallback2);
            // die();
            if ($fallback2 && \count($fallback2) > 0) {
                // $return = \Contao\Controller::getContentElement(array_shift($fallback2)['id']);
                return $this->renderContentElementOfSection($objSection, array_shift($fallback2)['id'], $customTemplate);
            }

            return false;
        }

        return false;
    }

    protected function renderModifiedModule($objRow, $strColumn = 'main', $wrap = false, $cssID = [])
    {
        // $this->framework->initialize();
        $strClass = \Module::findClass($objRow->type);

        // Return if the class does not exist
        if (!class_exists($strClass)) {
            \Contao\System::log('Module class "'.$strClass.'" (module "'.$objRow->type.'") does not exist', __METHOD__, TL_ERROR);

            return '';
        }

        /** @var Module $objModule */
        $objModule = new $strClass($objRow, $strColumn);
        $strBuffer = $objModule->generate();

        // return $strBuffer;
        // HOOK: add custom logic
        if (isset($GLOBALS['TL_HOOKS']['getFrontendModule']) && \is_array($GLOBALS['TL_HOOKS']['getFrontendModule'])) {
            foreach ($GLOBALS['TL_HOOKS']['getFrontendModule'] as $callback) {
                // do not run again on itself
                if ('pliigo.gbls.listeners.render_inserttags' === $callback[0]) {
                    continue;
                }
                $strBuffer = \Contao\Controller::importStatic($callback[0])->{$callback[1]}($objRow, $strBuffer, $objModule);
            }
        }

        if (false !== $wrap) {
            $cssID = deserialize($cssID, true);

            $cssID[0] = $cssID[0] ? " id=\"{$cssID[0]}\"" : '';
            $cssID[1] = $cssID[1] ? " class=\"{$cssID[1]}\"" : '';

            // print_r($cssID);
            // die();
            // return `<div>xxx</div>`;
            $strBuffer = "<div{$cssID[0]}{$cssID[1]}>{$strBuffer}<div>";
        }

        // Disable indexing if protected
        if ($objModule->protected && !preg_match('/^\s*<!-- indexer::stop/', $strBuffer)) {
            $strBuffer = "\n<!-- indexer::stop -->".$strBuffer."<!-- indexer::continue -->\n";
        }
        // return $strBuffer;

        $theme = $objRow->getRelated('pid');

        // Add start and end markers in debug mode
        if (\Contao\Config::get('debugMode')) {
            //    \Contao\Controller::loadLanguageFile("tl_module");

            $strRelPath = "\n".'module type: "'.\Contao\StringUtil::decodeEntities($objRow->type).'", '."\n".'module name: "'.\Contao\StringUtil::decodeEntities($objRow->name).'",'."\n".'module id: '.$objRow->id.', '."\n".'module in theme: "'.$theme->name.'", '."\n".'language variation used: "'.($objRow->pliigo_language ? $objRow->pliigo_language : '*').'"';
            // if ($template) {
            //     $strBuffer = "\n<!-- GLOBAL_LAYOUT_SECTION: USE CUSTOM TEMPLATE \"" . $template . "\" -->\n" . $strBuffer;
            // }
            $returner = "\n\n<!-- GLOBAL_LAYOUT_SECTION MODULE START: $strRelPath \n-->\n$strBuffer\n<!-- GLOBAL_LAYOUT_SECTION MODULE END -->\n\n";
        } else {
            $returner = $strBuffer;
        }
        // $strBuffer = "<!-- custom template:  " . $template . " -->\n" . $strBuffer;
        return $returner;
    }

    /**
     * Undocumented function.
     *
     * @param int|string $sectionId
     * @param mixed      $template
     * @param mixed      $disableCache
     * @param mixed      $objSection
     * @param mixed      $cteID
     *
     * @return string
     */
    protected function renderContentElementOfSection($objSection, $cteID, $template = '')
    {
        if (!$cteID || !$cteID > 0) {
            if (\Contao\Config::get('debugMode')) {
                $strRelPath = '(group: "'.$objSection->name_group.'", name: "'.$objSection->name.'", id: '.$objSection->id.', cte id: '.$objCte->id.', cte language: "'.$objCte->pliigo_gbl_sections_lang.'")';
                $strBuffer .= '<div class="message info" style="color: blue; border: 2px solid blue; background-color: rgba(0,0,255,0.1); padding: 1rem; margin: 1rem; display: block;"> DEBUG: Info for GLOABAL LAYOUT SECTION: no matching element found</div>';
            } else {
                $strBuffer = '<!-- no matching element found -->';
            }

            if ($template) {
                try {
                    $tpl = \Contao\Controller::getTemplate($template);
                } catch (\Exception $e) {
                    // $strBuffer .= "\n<!-- ".$e->getMessage()." -->";

                    if (\Contao\Config::get('debugMode')) {
                        $strRelPath = ''; //'(group: "'.$objSection->name_group.'", name: "'.$objSection->name.'", id: '.$objSection->id.', cte id: '.$objCte->id.', cte language: "'.$objCte->pliigo_gbl_sections_lang.'")';
                        $strBuffer .= '<div class="message error" style="color: red; border: 2px solid red; background-color: rgba(255,0,0,0.1); padding: 1rem; margin: 1rem; display: block;"> DEBUG: Error for GLOABAL LAYOUT SECTION: '.$e->getMessage().'<br>'.$strRelPath.''.'</div>';
                    } else {
                        $strBuffer .= '<!-- '.$e->getMessage()." $strRelPath -->";
                    }
                }
            }
        } else {
            $objCte = ContentModel::findByPk($cteID);
            if ($template) {
                $objCte->customTpl = $template;
            }

            try {
                $strBuffer = \Contao\Controller::getContentElement($objCte);
            } catch (\Exception $e) {
                if (\Contao\Config::get('debugMode')) {
                    $strRelPath = '(group: "'.$objSection->name_group.'", name: "'.$objSection->name.'", id: '.$objSection->id.', cte id: '.$objCte->id.', cte language: "'.$objCte->pliigo_gbl_sections_lang.'")';
                    $strBuffer = '<div class="message error" style="color: red; border: 2px solid red; background-color: rgba(255,0,0,0.1); padding: 1rem; margin: 1rem; display: block;"> DEBUG: Error for GLOABAL LAYOUT SECTION: '.$e->getMessage().'<br>'.$strRelPath.''.'</div>';
                } else {
                    $strBuffer = '<!-- '.$e->getMessage()." $strRelPath -->";
                }
                // \Contao\Message::addError($e->getMessage, "GLOBAL LAYOUT SECTIONS");
            }
        }

        if ($objSection->wrap_in_div) {
            $cssID = deserialize($objSection->cssID, true);
            $strBuffer = '<div'.($cssID[0] ? ' id="'.$cssID[0].'"' : '').($cssID[1] ? ' class="'.$cssID[1].'"' : '').'>'.$strBuffer.'</div>';
        }

        // Add start and end markers in debug mode
        if (\Contao\Config::get('debugMode')) {
            $strRelPath = '(group: "'.$objSection->name_group.'", name: "'.$objSection->name.'", id: '.$objSection->id.', cte id: '.$objCte->id.', cte language: "'.$objCte->pliigo_gbl_sections_lang.'")';
            if ($template) {
                $strBuffer = "\n<!-- GLOBAL_LAYOUT_SECTION: USE CUSTOM TEMPLATE \"".$template."\" -->\n".$strBuffer;
            }
            $returner = "\n\n<!-- GLOBAL_LAYOUT_SECTION START: $strRelPath -->\n$strBuffer\n<!-- GLOBAL_LAYOUT_SECTION END: $strRelPath -->\n\n";
        } else {
            $returner = $strBuffer;
        }
        // $strBuffer = "<!-- custom template:  " . $template . " -->\n" . $strBuffer;
        return $returner;
    }
}
